\documentclass[mathserif,18pt]{beamer}
 
\usepackage[utf8]{inputenc}

\beamertemplatenavigationsymbolsempty

% \setbeamercolor*{title}{fg=black}
\setbeamerfont{frametitle}{series=\bfseries}
% \setbeamercolor{frametitle}{fg=black}
% \setbeamerfont{block title}{series=\bfseries}
% \setbeamercolor{block title}{fg=black}

% \setbeamertemplate{title page}[default][left,colsep=-4bp,rounded=true,shadow=\beamer@themerounded@shadow]

\setbeamersize{text margin left=0.5cm, text margin right=0.5cm}

\setbeamerfont{footline}{series=\bfseries, size=\large}
%
\addtobeamertemplate{navigation symbols}{}{%
  \usebeamerfont{footline}%
  \usebeamercolor[fg]{footline}%
  \hspace{1em}%
  \insertframenumber/\inserttotalframenumber
}

\usepackage{appendixnumberbeamer}

\settowidth{\leftmargini}{\usebeamertemplate{itemize item}}
\addtolength{\leftmargini}{\labelsep}

\usepackage{booktabs}

\usepackage{pgfplots}%
\pgfplotsset{compat=1.7}%
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,angles,quotes,arrows,shadows,fit,calc,positioning,decorations.pathmorphing,shapes.geometric}
\usepackage{tikzpeople}
\usepackage{forest}

\usepackage{graphbox}

\usepackage[english]{babel}
\usepackage{csquotes}

\colorlet{darkgreen}{green!50!black}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{lmodern}

\usepackage[style=alphabetic,]{biblatex}
\addbibresource{../papier-dexa17-short-CR/crowdguard-rndresp-VLIGHT.bib}
\addbibresource{../papier-dexa17/crowdguard-rndresp-VLIGHT.bib}
\addbibresource{main.bib}
%
\setbeamertemplate{bibliography item}{\insertbiblabel}
%
\renewcommand*{\bibfont}{\tiny}
\setbeamerfont{bibliography item}{size=\tiny}
\setbeamerfont{bibliography entry author}{size=\tiny}
\setbeamerfont{bibliography entry title}{size=\tiny}
\setbeamerfont{bibliography entry location}{size=\tiny}
\setbeamerfont{bibliography entry note}{size=\tiny}

\newcommand{\eg}[1]{{\footnotesize\usebeamercolor[fg]{block title example}{\(\blacktriangleright\)} {#1}}}

\newcommand{\todo}[1]{{\color{blue}\textbf{TODO}: #1}}

\renewcommand{\alert}[1]{{\color{red}\textbf{#1}}}

\title{\textbf{Lightweight Privacy-Preserving Task Assignment in
    Skill-Aware Crowdsourcing}}
%
\author[L. Béziaud, T. Allard, D. Gross-Amblard]{%
  \underline{Louis Béziaud}\inst{1,3}%
  \and%
  Tristan Allard\inst{1,2}%
  \and%
  David Gross-Amblard\inst{1,2}%
}
%
\institute[Univ. Rennes 1, IRISA, ENS Rennes]{%
  \inst{1}~Univ. Rennes 1, Rennes, France %
  \and %
  \inst{2}~IRISA, Rennes, France\quad \texttt{first.last@irisa.fr}
  \and %
  \inst{3}~ENS Rennes, Rennes, France\quad
  \texttt{first.last@ens-rennes.fr} %
}
% 
\date[DEXA'17]{DEXA'17 --- August 30, 2017}
%
\titlegraphic{%
  \includegraphics[height=24pt,align=b]{logo-anr}\hspace*{10pt}%
  % \includegraphics[height=24pt,align=b]{logo-druid-noir}\hspace*{10pt}%
  \includegraphics[height=24pt,align=b]{cropped-bannerDruid.png}\hspace*{10pt}%
  \includegraphics[height=24pt,align=b]{logo-Irisa-couleur}\hspace*{10pt}%
  \includegraphics[height=24pt,align=b]{logo-Universite_Rennes_1}\hspace*{10pt}%
  \includegraphics[height=24pt,align=b]{Logo_ENS_bleu_print}%
  \\%
  {\tiny Supported by the ANR grant ANR-16-CE23-0004. \emph{CROWDGUARD} \url{crowdguard.irisa.fr}}
}

\begin{document}
 
\frame{\titlepage}

% \begin{frame}
%   \frametitle{Outline}
%   \tableofcontents
% \end{frame}

\begin{frame}{Crowdsourcing}
  \begin{block}{}\vspace{-18pt}
    \textcquote{howe2006rise}{%
      Crowdsourcing represents the act of a company or institution
      taking a function once performed by employees and
      \alert{outsourcing} it to an \alert{undefined} (and generally
      \alert{large}) network of people in the form of an open call%
    }

    \eg{Amazon Mechanical Turk: crowdsourcing marketplace}\\
    \eg{Wikipedia: writing encyclopedia articles}\\
    \eg{Galaxy Zoo: classification of galaxies}
  \end{block}
  
  \bigskip

  % \visible<2>{
  \begin{block}{Knowledge-Intensive
      Crowdsourcing~\cite{BasuRoy2015}}%
    Some tasks \alert{require} particular \alert{skills} to be
    completed

    \eg{testing an application on a specific device}\\
    \eg{a translation from English to French}\\
    \eg{programming in \texttt{C++}}
  \end{block}
  % }
\end{frame}

\begin{frame}{Privacy in Crowdsourcing?}
  \begin{block}{Knowledge \(\implies\) Privacy Issues}
    Skills can be \alert{quasi-identifiers} / \alert{sensitive data} :\\
    \eg{unique combination of skill, location, availability, wage, specific device, ...}
  \end{block}

  \bigskip
  
  The platform is \alert{not a trusted third party} (negligence, illegitimate use, external attack)

  \bigskip
  
  \begin{block}{Example of Data Breach}
    \eg{A worker ID on mTurk gives access to the Amazon profile: real name, wish lists, book reviews, tagged products, ...~\cite{LeaseHullmanBighamEtAl2013}}\\
    \eg{An Uber's executive illegitimately tracked a journalist's location~\cite{bhuiyan2014god}}\\
    \eg{Ashley Madison dating service's user base was stolen~\cite{mansfield2015ashley}}
  \end{block}
\end{frame}

\begin{frame}{Related Work}
  \begin{itemize}
  \item Optimal task assignment using a \alert{decentralized} maximum
    flow algorithm inside a Paillier \alert{homomorphic
      cryptosystem}~\cite{Kajino2016}
    
    {\usebeamercolor[fg]{block title}\(\Rightarrow\)} need
    \alert{more than a century} to assign 100 workers and tasks,
    according to the author

  \item Privacy-preserving crowdsourced
    surveys~\cite{KandappuSivaramanFriedmanEtAl2014} or spatial
    crowdsourcing~\cite{ToGhinitaShahabi2014}
    
    {\usebeamercolor[fg]{block title}\(\Rightarrow\)} \alert{no
    skills}, \alert{no assignment}
  \end{itemize}
  
  \bigskip
  
  How can we do \alert{privacy-preserving} task assignment with
  \alert{lightweight techniques} (i.e. no encryption, centralized)?
\end{frame}

\begin{frame}[c]{Approach}
  \begin{center}
    \resizebox{!}{6cm}{%
      % \includegraphics[height=0.7\textheight]{../papier-dexa17/figs/introarchi.pdf}
      \begin{tikzpicture}[
        node distance=2,
        % 
        arrow/.style={thick,->,>=stealth},
        ]
        % 
        \node[bob, minimum size=1cm,label=south:{\bf Worker}] (worker) {};
        % 
        \node[below=of worker, alice, minimum size=1cm,label=south:{\bf Requester}] (requester) {};
        % 
        \coordinate (pre_platform) at ($(worker)!0.5!(requester)$);
        \node[right=5cm of pre_platform, monitor, minimum size=1cm, label=south:{\bf Platform}] (platform) {\includegraphics[width=1cm]{server}};
        \coordinate[right=2.5cm of platform] (post_platform);
        %
        \only<1>{
          \draw [arrow] (worker) -- (platform) node[above,sloped,midway] {profile};
        }
        \only<2>{
          \node[draw, double, rounded corners, fill=red!20] (protection) at ($(worker)!0.5!(platform)$) {protection};
          \draw [arrow] (worker) -- (protection) node[above,sloped,midway] {profile};
          \draw [arrow] (protection) -- (platform) node[above,sloped,midway,align=center] {protected\\profile};
        }
        \draw [arrow] (requester) -- (platform) node[below,sloped,midway] {requirements};
        %
        \draw [arrow] (platform) -- (post_platform) node[above,sloped,midway] {assignment};
      \end{tikzpicture}
    }
  \end{center}
\end{frame}


\begin{frame}{Skills Profile}
  \begin{block}{Skills Taxonomy}
    Skills are organized in a tree-structure with a \emph{is-a}
    relationship.
  \end{block}

  \begin{center}
    \resizebox{0.8\linewidth}{!}{%
      \begin{tikzpicture}[
        taxo/.style={
          shape=rectangle,
          rounded corners,
          draw,
          align=center,
          top color=white,
          bottom
          color=black!10
        },
        auto,
        level 1/.style={sibling distance=9em},
        level 2/.style={sibling distance=5em},
        ]
        \node[taxo] {$\perp$} child {
          node[taxo] (Sport) {Sport} child {
            node[taxo] (Yoga) {Yoga} child {
              node[taxo] (YogaH) {Hata Yoga}} child {
              node[taxo] (YogaO) {\ldots}
            }
          } child {
            node[taxo] (SportO) {\ldots}
          }
        } child {
          node[taxo] (CS) {CS} child {
            node[taxo] (Enc) {Crypto.}  child {
              node[taxo] (EncFHE) {FHE}
            } child {
              node[taxo] (EncO) {\ldots}
            }
          } child {
            node[taxo] (CSO) {\ldots}
          }
        } child {
          node[taxo] (H) {Health} child {
            node[taxo] (HAM) {Alt.}  child {
              node [taxo] (HAMO) {\ldots}
            } child {
              node [taxo] (HAMC) {Chemo. relief}
            }
          } child {
            node[taxo] (HO) {\ldots}
          }
        };
        %
        \node[align=left,anchor=west] at (-7, 0) (superskills) {\textbf{super-skills}};
        \node[align=left,anchor=west] at (-7, -3.9) (skills) {\textbf{skills}};
        %
        \node[draw,dashed,inner sep=0.1cm,fit={
          (skills) (YogaH) (YogaO) (EncFHE) (EncO) (HAMO) ($(HAMC.east)+(4pt,0)$)
        }] (skills) {};
        \node[draw,dashed,inner sep=0.1cm,fit={
          (superskills) (CSO) (Sport) (Yoga) (SportO) (CS) (Enc) (H) (HO) (HAM)}] (superskills) {};
      \end{tikzpicture}
    }
  \end{center}

  \begin{block}{Profile}
  \begin{itemize}
  \item \textbf{skill} \(s_i \in Boolean\) % \{0, 1\}
  \item \textbf{super-skill} \(s_i =\) proportion of descendant skills possessed
  \item[\(\Rightarrow\)] \textbf{profile} = array of \textbf{skills}
  \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Differential Privacy~\cite{DBLP:conf/icalp/Dwork06}}
  \begin{block}{Idea}
    The \alert{outcome of any analysis is essentially equally likely}, independent of whether an individual joins, or refrains from joining, the data set
  \end{block}

  \begin{block}{Differential Privacy~\cite{DBLP:conf/icalp/Dwork06}}
    \(M\) gives \(\epsilon\)-differential privacy if for all pairs of data-sets \(x\), \(y\) differing in one element, and all subsets \(S\) of possible outputs, \[\Pr[M(x) \in S] \leq e^\epsilon \Pr[M(y) \in S]\]
  \end{block}
  
  \begin{block}{Properties}
    \begin{itemize}
      \item Sequential \& parallel \alert{composition}~\cite{DBLP:conf/sigmod/McSherry09} \(\implies\) budget \(\epsilon\) sharing
      \item Post-processing~\cite{DworkRoth2014} \(\implies\) can re-use data and add information
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Task Assignment Problem}
  \begin{block}{Assignment problem}
    \begin{itemize}
    \item\textbf{workers} \(\mathcal{P} = \{p_0, p_1, ..., p_n\}\)
    \item\textbf{tasks} \(\mathcal{T} = \{t_0, t_1, ..., t_n\}\)
    \item\textbf{cost function} \(C \colon \mathcal{P} \times \mathcal{T} \to \mathbb{R}\)
    \end{itemize}

    Find a bijection \(f\colon \mathcal{P} \to \mathcal{T}\) such that \(\sum_{p\in \mathcal{P}} C(p, f(p))\) is minimized.

    w.l.o.g. assumes \(|\mathcal{P}| = |\mathcal{T}|\)
  \end{block}
   
  \begin{block}{}
    simplex algorithm, Hungarian algorithm, minimum-cost flow, ...
  \end{block}
\end{frame}

\begin{frame}{Assignment Quality}
  Assignment's total cost depends on the weight-function

  \bigskip
  
  \begin{block}{Relative Quality}
    \[
      \mathtt{q_{rel}} %(\widetilde{\mathcal{A}}, \mathcal{A})
      =
      \frac{\sum_{{(t,p) \in \mathcal{A}}} \mathtt{C} (t,p)}
      {\sum_{{(t,\tilde{p}) \in \tilde{\mathcal{A}}}}
        \mathtt{C} (t,\tilde{p})}
    \]
  \end{block}

  \begin{block}{Fraction of Perfect Assignments}
    \[
      \mathtt{fpa} %(\widetilde{\mathcal{A}}, \mathcal{A})
      = \frac{\left\lvert \left\{ \tilde{p} \geq t \mid (t,\tilde{p}) \in \tilde{\mathcal{A}} \right\} \right\rvert}{\lvert\tilde{\mathcal{A}}\rvert}
    \]
  \end{block}
\end{frame}

\begin{frame}{Randomized Response~\cite{Warner1965}}
  % \begin{figure}[h]
  %   \centering
  %   \resizebox{!}{0.7\textheight}{% minimum width=3cm, minimum height=0.5cm
  %     \tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
  %     \tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=green!30]
  %     \tikzstyle{arrow} = [thick,->,>=stealth]
  %     \begin{tikzpicture}[node distance=4cm,every node/.style={font=\large},]
  %       \node (in1) [io] {\(x\)};
  %       \node (dec1) [decision, below of=in1] {\(\mathtt{rand}() > \Pr_{flip}\)};
  %       \node (out1) [io, right of=dec1, xshift=2cm] {\(x\)};
  %       \node (dec2) [decision, below of=dec1] {\(\mathtt{rand}() > \Pr_{inno}\)};
  %       \node (out2) [io, right of=dec2, xshift=2cm] {\(0\)};
  %       \node (out3) [io, below of=dec2] {\(1\)};
  %       %
  %       \draw [arrow] (in1) -- (dec1);
  %       \draw [arrow] (dec1) -- node[anchor=north] {yes} (out1);
  %       \draw [arrow] (dec1) -- node[anchor=east] {no} (dec2);
  %       \draw [arrow] (dec2) -- node[anchor=north] {yes} (out2);
  %       \draw [arrow] (dec2) -- node[anchor=east] {no} (out3);
  %     \end{tikzpicture}
  %   }

  Proposed by S.~L.~Warner in 1965 for \alert{survey interviews}. It
  allows respondents to respond to sensitive issues while maintaining
  confidentiality.%
  \[
    \widetilde{x} = \begin{cases}
      x &\text{with probability\quad} 1-\Pr_{flip} \\
      1 &\text{\phantom{with probability\quad}} 0.5 \times \Pr_{flip} \\
      0 &\text{\phantom{with probability\quad}} 0.5 \times \Pr_{flip} \\
    \end{cases}
  \]

  % \bigskip
  
  \textbf{Th.} randomized response satisfies
  \(\epsilon\)-differential-privacy if
  \(\textrm{Pr}_{flip} = \frac{2}{1 + e^\epsilon}\)

  \begin{center}
    \begin{forest}
      [0.75, tikz={\node [inner sep=1pt,draw,dotted,fit=(l1)(l2)(l3)(l4)] {};}
      [1
      [1,name=l1]
      [1,name=l2]
      ]
      [0.5
      [1,name=l3]
      [0,name=l4]
      ]
      ]
    \end{forest}
    \(\xrightarrow{\text{randomized response}}\)
    \begin{forest}
      [0.75, tikz={\node [inner sep=1pt,draw,dotted,fit=(l1)(l2)(l3)(l4)] {};}
      [0.5
      [1,name=l1]
      [0,name=l2]
      ]
      [1
      [1,name=l3]
      [1,name=l4]
      ]
      ]
    \end{forest}
  \end{center}
\end{frame}

\begin{frame}{Existing Weight Functions}
  \begin{block}{Missing Weight Function \footnotesize{(\(\sim\) Hamming)}}
    number of skills required by the task and missing in the worker's profile
    \[
      \mathtt{MWF} (t, \tilde{p}) =
      \sum_{i} t[i] \land \neg \tilde{p}[i]
    \]
  \end{block}

  \begin{block}{Ancestors Weight Function \footnotesize{(from~\cite{MavridisGross-AmblardMiklos2016})}}
    distance between the depth of each worker's skill and
    its closest required skill
    \[
      \mathtt{AWF}(t, \tilde{p}) = {
        \sum_{s_i \in \tilde{p}} {
          \min_{s_j \in t} (\mathrm{depth}(\mathrm{lca}(s_i, s_j)))
        }
      }
    \]
    {\footnotesize \(d_{max}\) = depth of the taxonomy}\\
    {\footnotesize \(\mathrm{lca}\) = lowest common ancestor}
  \end{block}
\end{frame}

\begin{frame}[t]{Climbing Weight Function \(\updownarrow\)}
  \begin{center}
    \resizebox{0.6\linewidth}{!}{%
      \input{climbing.tikz}
    }
  \end{center}

  \alert{Idea 1} leveraging the \emph{is-a} relationship by computing a distance for each level

  \alert{top = more precision, less relevance\\ bottom = less precision, more relevance}
  
  {
    \[
      \mathtt{CWF} (t, \tilde{p}) = \sum_{\text{~level~} i} \textcolor{green}{i \times} \mathtt{d} (\textcolor{blue}{u_i}, \textcolor{red}{v_i})
    \]
    {\(\mathtt{d}\) = \alert{any weight function}}
  }
\end{frame}

\begin{frame}[t]{Touring Weight Function \(\leftrightarrow\)}
  \begin{center}
    \resizebox{0.4\linewidth}{!}{%
      \input{touring.tikz}
    }
  \end{center}

  \alert{Idea 2} leveraging the ``neighborhood'' property: two \alert{close skills are more likely to have the same value} than two distant skills
  
  {
    \[
      \mathtt{TWF} (t, \tilde{p}) =
      % \frac{1}{|t| \times |\tilde{p}|} 
      \sum_{\textcolor{blue}{s_i} \in t}\ \sum_{\textcolor{red}{s_j} \in \tilde{p}}\ ( s_i \textcolor{green}{\leadsto} s_j )
    \]
    {\footnotesize \(\leadsto\) = distance in the taxonomy tree}
  }
\end{frame}

\begin{frame}{Results (subset)}
  % \only<1>{
    \begin{itemize}
      \tiny
    \item taxonomy: perfect tree, height = 3, branching = 4, 121 nodes, 81 skills
    \item \texttt{Bernoulli1}: user has skill \(s_i\) with probability 0.1 (10\%)
    \item \texttt{Bernoulli01}: user has skill \(s_i\) with probability 0.01 (1\%)
    \item \texttt{Normal}: skills are drawn from a normal distribution (around 30\%)
    \item 100 workers, 100 tasks%
    \end{itemize}
  % }
  
  \begin{center}
    \includegraphics[width=0.9\linewidth]{{../papier-dexa17/figs/fig6.qrel}.png}
    \includegraphics[width=0.9\linewidth]{{../papier-dexa17/figs/fig6.qabs}.png}
  \end{center}

  % \only<2>{
  %   \begin{itemize}
  %   \item optimal unperturbed assignment (cost = 0) \(\implies \mathrm{q_{rel}} = 0\)
  %   \item Hamming 
  %   \end{itemize}
  % }
\end{frame}

% \begin{frame}{Results}
%   \begin{itemize}
%     \tiny
%   \item taxonomy: Skill-Project, 
%   \item \texttt{Bernoulli1}: user has skill \(s_i\) with probability 0.1 (10\%)
%   \item \texttt{Bernoulli01}: user has skill \(s_i\) with probability 0.01 (1\%)
%   \item \texttt{Normal}: skills are drawn from a normal distribution (around 30\%)
%   \item 100 workers, 100 tasks%
%   \end{itemize}

%   \begin{center}
%     \includegraphics[width=0.4\linewidth]{{../papier-dexa17/figs/fig7.qrel}.png}%
%     \includegraphics[width=0.4\linewidth]{{../papier-dexa17/figs/fig7.qabs}.png}
%   \end{center}
%   % \includegraphics[width=\linewidth]{../papier-dexa17/figs/fig7.png}%
% \end{frame}

\begin{frame}{Conclusion}
  \begin{itemize}
  \item \alert{privacy-preserving} approach to the problem of
    assigning tasks to workers with new \alert{weight functions}
  \item \alert{lightweight}
  \item \alert{client-side}
  \item \alert{pluggable} into existing platforms
  \end{itemize}

  \begin{exampleblock}{Future Work}
    \begin{itemize}
    \item large-scale real-life skill dataset
    \item complete skills taxonomy {\tiny (\emph{e.g.} Skill-Project \url{skill-project.org})}
    \item performance vs quality trade-off \(\leftrightarrow\) mix perturbation + encryption
    \item does client-side differential privacy make sens?
    \end{itemize}
  \end{exampleblock}

  \only<2>{%
    \centering%
    \large%
    \colorbox{title.fg}{%
      \textcolor{white}{%
        \textbf{Thank you!} Questions? \texttt{\url{louis.beziaud@ens-rennes.fr}}%
      }%
    }%
  }%
\end{frame}

\appendix

% \begin{frame}{Taxonomies}
%   \begin{itemize}
%   \item Synthetic taxonomies: perfect trees height 3, and branching
%     factor of 4, containing 121 nodes of which 81 are leaves

%     \item \alert{Real} taxonomy from
%       Skill-Project\footnote{\url{http://en.skill-project.org/skills/}}:
%       2208 nodes, 1562 skills

%       \begin{center}
%         \includegraphics[height=0.5\textheight]{../papier-dexa17/figs/skillproject} 
%       \end{center}
%     \end{itemize}
% \end{frame}

\begin{frame}{Backup: \texttt{Normal} skills profile}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{../papier-dexa17/figs/normal81.png}
  \end{center}

  draw skills from \(\mathcal{N}(\sqrt{r}\cos\theta, \sqrt{r}|\sin\theta|)\) with \(\theta\in [0, 2\pi)\) for each profile, and \(r=0.06\). around 30\% of skills
\end{frame}

\begin{frame}{Backup: Time}
  \begin{center}
    \setlength{\tabcolsep}{8pt}
    \begin{tabular}{lrr}
      \toprule
      Weight function & Time (s) & Time complexity  \\
      \midrule
      \texttt{rand}    & 0.00099 & \(\mathcal{O}(1)\) \\
      \texttt{hamming} & 0.02467 & \(\mathcal{O}(\lvert\mathcal{S}\rvert)\) \\
      \texttt{MWF}     & 0.01969 & \(\mathcal{O}(\lvert\mathcal{S}\rvert)\) \\
      \texttt{CWF}     & 0.30395 & \(\mathcal{O}(\lvert\mathcal{S}^T\rvert)\) \\
      \texttt{AWF}     & 1.99307 & \(\mathcal{O}(\lvert\mathcal{S}\rvert^2)\) \\
      \texttt{TWF}     & 2.96748 & \(\mathcal{O}(\lvert\mathcal{S}\rvert^2)\) \\
      \bottomrule
    \end{tabular}
  \end{center}

  Skill-project, 2208 nodes, 1562 skills
\end{frame}

\begin{frame}{Backup: \(\Pr_{flip}\) versus \(\epsilon\)}
  \begin{center}
  \resizebox{0.7\linewidth}{!}{%
    \begin{tikzpicture}
      \begin{axis}[%
        axis x line=left,%
        axis y line=left,%
        xmode=log,%
        clip=true,%
        xmajorgrids=true,%
        ymajorgrids=true,%
        ylabel={Flipping probability},%
        xlabel={Privacy budget},%
        ymin=0,%
        ymax=1,%
        ]%
        % \addplot+[draw=black,mark=none,samples=500,domain=0.01:10]{1/(exp(x)+1)};%
        \addplot+[draw=black,mark=none,samples=500,domain=0.01:10]{2/(exp(x)+1)};% innocuous version
        \draw[thick,decoration={brace,raise=7pt},decorate] %
        (axis cs:0.01,0) -- node[above=9pt] {common \(\epsilon\)}
        (axis cs:2,0);
      \end{axis}
    \end{tikzpicture}
  }%
  \end{center}
\end{frame}

\begin{frame}[allowframebreaks]{References}
  \printbibliography%
  % \bibliographystyle{plain}
\end{frame}

\end{document}

