\section{Evaluation}%
\label{sec:eval}%
% This section describes the settings of our experiments, presents the
% performance results, and details the quality results.

\subsection{Experimental Settings}

We evaluated the performance and quality reached by the
\texttt{MWF}, \texttt{CWF}, and \texttt{TWF} weight functions. We
implemented several comparison baselines: the \texttt{Hamming} weight
function, the \texttt{AWF} weight function, and a random
matching. Performance is measured in terms of wall-clock time on a
non-optimized implementation, and quality is measured according to the
Definitions~\ref{def:qualrel} and~\ref{def:fpa}.

We use two skills taxonomies. (1) A real-life taxonomy
(\texttt{Skill\-Project}), assembled from Skill-Project, the
open-source skills collection project. \texttt{Skill\-Project}
contains 1562 skills, and 2208 nodes whose distribution is given in
Figure~\ref{fig:skillproject}. (2) A synthetic taxonomy
(\texttt{Per\-fect34}) that we generated from a perfect balanced tree
of height 3 and branching factor 4, thus containing 85 nodes of which
64 are leaves.

\begin{figure}[!t]
  \centerfloat%
  \begin{minipage}[b][][b]{.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figs/skillproject}
    \caption{Height and degree distributions of the \texttt{SkillProject} taxonomy}%
    \label{fig:skillproject}
  \end{minipage}
  \hfill
  \begin{minipage}[b][][b]{.5\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figs/normal81}
    \caption{Skills distribution of the \texttt{Nor\-mal} dataset over
      the \texttt{Per\-fect34} taxonomy}%
    \label{fig:normal81}
  \end{minipage}
\end{figure}

Our experiments were performed over three synthetic datasets. (1) In
the first dataset (\texttt{Normal}), the skills (workers and tasks)
are randomly sampled from the leaves of the taxonomy following a
normal distribution \(\mathcal{N}(\sqrt r \cos \theta, \sqrt r
\lvert\sin \theta\rvert)\) with \(\theta\) randomly chosen in \([0,\ 2
\pi)\) for each profile, and \(r=0.06\), corresponding to around 30\%
of skills per profile.  The resulting distribution is given on
Figure~\ref{fig:normal81}. (2) In the two other datasets, the skills
of each worker's and task's profile are sampled uniformly at random in
the leaves of the taxonomy at varying rates. We call
\texttt{Bern\-oulli01} the dataset drawn from \(\mathcal{B}(1,
p=0.01)\), and \texttt{Bern\-oulli1} the one from \(\mathcal{B}(1,
p=0.1)\). All the assignments were made between 100 workers and 100
tasks.

The experiments presented in this paper were carried out using the
Grid'5000 testbed\footnote{\url{https://www.grid5000.fr}}. All
experiment were conducted on machines running Debian Jessie (64-bits)
with an Intel(R) Xeon(R) E5-2680 v4 at 2.4 GHz and 16GB of RAM. The
code was written in Python\footnote{\url{http://www.python.org}} 3.5.3
using NumPy\footnote{\url{http://www.numpy.org}} 1.12.0,
SciPy\footnote{\url{https://www.scipy.org}} 0.18.1,
NetworkX\footnote{\url{https://networkx.github.io}} 1.11, and
JobLib\footnote{\url{https://pythonhosted.org/joblib}}
0.10.4.dev0. Every experiment was run three times, unless stated
otherwise.


\subsection{Performance}%
\begin{table}[tbhp]
  \begin{center}
    \setlength{\tabcolsep}{8pt}
    \begin{tabular}{lrr}
      \toprule
      Weight function & Time (s) & Time complexity  \\
      \midrule
      \texttt{rand}    & 0.00099 & \(\mathcal{O}(1)\) \\
      \texttt{hamming} & 0.02467 & \(\mathcal{O}(\lvert\mathcal{S}\rvert)\) \\
      \texttt{MWF}     & 0.01969 & \(\mathcal{O}(\lvert\mathcal{S}\rvert)\) \\
      \texttt{CWF}     & 0.30395 & \(\mathcal{O}(\lvert\mathcal{S}^T\rvert)\) \\
      \texttt{AWF}     & 1.99307 & \(\mathcal{O}(\lvert\mathcal{S}\rvert^2)\) \\
      \texttt{TWF}     & 2.96748 & \(\mathcal{O}(\lvert\mathcal{S}\rvert^2)\) \\
      \bottomrule
    \end{tabular}
  \end{center}
  \caption{Wall-Clock Time of a Call to each Weight Function}\label{tab:time}
  % over 1000 runs
  \vspace{-5pt}
\end{table}

Our approach does not generate any network overhead since a perturbed
skills vectors has the same length as an original skills
vector\footnote{We note that though the length of a skills vector is
  unchanged, the \textsc{Flip} mechanism could increase the entropy,
  thus reducing compressing capabilities.}. We consequently focus on
the wall-clock time taken to compute the weight
functions. Table~\ref{tab:time} shows the average time among one
thousand calls to each weight function, as well as their asymptotic
complexities. The taxonomy is \texttt{Perfect34} and the dataset
\texttt{Bernoulli1}. We assume pre-computed lowest-common-ancestors
and shortest-paths, and that the distance used by \texttt{CWF} is
linear in time. We observe that these times are insignificant with
respect to typical computation times of the assignment algorithm.

\subsection{Quality}

\begin{figure}[bthp]
  \centerfloat%
  \includegraphics[width=1.2\textwidth]{{figs/fig6.qrel}.png}
  \includegraphics[width=1.2\textwidth]{{figs/fig6.qabs}.png}
  \caption{\(\mathtt{q_{rel}}\) and \(\mathtt{fpa}\) for each weight function, with respect to \(\Pr_{flip}\), for the \texttt{Perfect34} taxonomy. \(\mathtt{fpa}\) of non-perturbated assignments is dashed}%
  \label{fig:qualSynt}%
\end{figure}

\begin{figure}[bthp]
  \centerfloat%
  \includegraphics[width=0.4\textwidth]{{figs/fig7.qrel}.png}~%
  \includegraphics[width=0.4\textwidth]{{figs/fig7.qabs}.png}
  \caption{\(\mathtt{q_{rel}}\) and \(\mathtt{fpa}\) for each weight function, with respect to \(\Pr_{flip}\), for the \texttt{SkillProject} taxonomy, and \texttt{Normal} distribution}%
  \label{fig:qualReal} 
\end{figure}

Figure~\ref{fig:qualSynt} shows the evolution of the two quality
measures $\mathtt{q_{rel}}$ and $\mathtt{fpa}$, according to the bit
flipping probability $\Pr_{flip}$, of the complete set of weight
functions and on the random matching, on the synthetic taxonomy
\texttt{Perfect34}. First, let consider the \texttt{Bernoulli}
datasets. We observe globally that the weight functions are rather
robust against the differentially-private perturbation.  \texttt{TWF}
exhibits a noticeable $\mathtt{q_{rel}}$ measure equal to $0$ on the
two \texttt{Bernoulli} datasets. This is actually due to the $0$ cost
of the unperturbed assignment which makes $\mathtt{q_{rel}}$ equal to
$0$ whatever the cost of the perturbed assignment. The later exhibited
a value similar to the cost of \texttt{TWF} on the \texttt{Normal}
dataset. Given its high relative quality, \texttt{Hamming} happens to
be a robust metric. However, its \texttt{fpa} score is $0$ (or close
to) on the two \texttt{Bernoulli} datasets. This is not the case for
the other metrics on \texttt{Bernoulli01}, especially for \texttt{MWF}
and \texttt{AWF} and except \texttt{TWF}. Second, on the normal
dataset the weight functions using the taxonomy (\emph{i.e.,}
\texttt{TWF}, \texttt{CWF}, \texttt{AWF}) have a relative quality
close to $1$ robust and achieve a reasonnable, similar, \texttt{fpa}
score. %
Figure~\ref{fig:qualReal} focuses on the real-life taxonomy
\texttt{SkillProject}. It depicts the evolution of the
$\mathtt{q_{rel}}$ and $\mathtt{fpa}$ metrics according to
$\Pr_{flip}$ of the various weight functions. All weight functions
show high $q_{rel}$ values and reasonable, similar, \texttt{fpa}
scores.




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "www2017"
%%% End: 
