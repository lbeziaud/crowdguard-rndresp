# Review details for : Lightweight Privacy-Preserving Task Assignment in Skill-Aware Crowdsourcing

## Review Mark Summary

| Criteria       | Weight | Review #1   | Review #2   | Review #3   | Review #4   |
|----------------+--------+-------------+-------------+-------------+-------------|
| Originality    |      2 | Weak reject | Accept      | Weak accept | Weak accept |
| Quality        |      2 | Weak reject | Accept      | Accept      | Neutral     |
| Relevance      |      1 | Accept      | Weak accept | Weak accept | Accept      |
| Presentation   |      1 | Weak accept | Accept      | Accept      | Neutral     |
| Recommendation |      6 | Weak reject | Accept      | Weak accept | Neutral     |
| **Overall**    |        | **3.41667** | **5.91667** | **5.25**    | **4.33333** |

## Review #1

**Author Comments**

The proof that the flip mechanism provides differential privacy needs
to be included in the paper. Having said that, I looked at the proof
in the technical report and it is not rigorous. The authors mention
that the flip probability is strictly lower than 0.5. But Fig. 3 shows
otherwise. Also, for the values used in the experiments (l = 1562
skills), the flip probability would be over 0.99 (even for a privacy
budget of 2). What is the effect of that on differential privacy? The
assumption that |T|=|P| is too restrictive. Typically, the number of
workers and tasks would be very different. If this is the case, is the
Hungarian method still applicable for the assignment problem or is
another solution required? The simulation experiments are very
limited. The authors only consider the case where |T|=|P|=100. They
should consider much larger values, in the order of tens of thousands.

## Review #2

**Author Comments**

In this paper, authors propose a privacy-preserving approach to the
problem of assigning tasks to the workers while protecting workers'
privacy. The proposed approach sstisfies differential privacy by
allowing workers to perturb their skill profile locally. Authors
propose weight functions which are capable of coping with
differentially private perturbation. Their experiments show that the
quality of the perturbed assignments get closer to the quality levels
of non-perturbed assignments. Authors could better motivate the reader
on the subject matter by presenting a real-life example or
scenario. The paper is well-written. The footnotes on the first page
start from #4 instead of #1.

## Review #3

**Author Comments**

The authors show how to implement the process of task assignment in a
privacy-preserving fashion using differential privacy. To this end,
workers publish "sanitized" versions of their skill sets. Overall I
like the approach of the paper. The submission is also well written
and tackles an interesting problem. I still have two suggestions to
improve the paper. First, the process of obtaining a differentially
private skill set is completely agnostic of the Skill Taxonomy, it
simply flips bits randomly. Intuitively, this may yield to a situation
where flipping one bit a a certain position has a more signifiant
impact on the utility of the data than flipping another bit. Could
this be mitigated if the taxonomy is used during sanitation? Second,
it is not clear to me how to intuitively interpret Figures 6 and 7:
while I do understand the math behind the metrics used, I wonder
whether they intuitively conform to our understanding of quality,
i.e., would the sanitised data sets really be useful in a certain
application context? The authors should argue better here.

## Review #4

**Author Comments**

- Summary: Task assignment is a key step in modern crowdsourcing
systems and mainly in skill aware applications. Despite being
effective, skill/profile based assignment can violate the privacy of
workers. This paper proposes a lightweight privacy preserving task
assignment as follow:
    1. Worker and task profiles are build using a unique skill
    taxonomy.
    2. Worker profiles are perturbed on the worker side through
    randomised response before being submitted to the platform.
    3. Worker to tasks matching is seen as a bijection finding problem
    with cost function (weight function)
- Context: Skill aware Crowdsourcing (wide definition: e.g. also
applications like Uber, home-related tasks…) 
- Problem: Privacy preserving 
- Method: Differential privacy using
skill taxonomy profiles 
- Objective: Computation cost reduction
- Positive points: 
    1. The problem is well described and the solution proposal is well
    motivated
    2. The method is clearly detailed
    3. Despite being common in other domains, the workflow is novel in
    the domain of crowdsourcing
- Negative points: 
    1. In motivations (minor): Authors give an example motivation for
    their work as follow: “g., a company may prefer that its employees
    do not participate to crowdsourced projects in order to avoid any
    risk of disclosures “. This is not valid as workers in
    crowdsourcing are usually anonymised from an external person point
    of view. The privacy issue mainly concerns the worker/platform and
    worker/requester combinations. 
    2. In assumptions: Authors makes multiple strong assumptions
    without showing why those assumption don’t affect the method
    output: 
        1. One requester - one task 
        2. Task set and worker set are of the same size: as
        crowdsourced contributions are usually meant to be aggregated
        from different workers and that worker may or may not choose
        the task it is essential to match the task. It is not shown
        how ranking can be handled in this generic case 
        3. Workers are honest-but-curious: this is not a wrong
        assumption as is. However, the problem raises from the fact
        that self estimated skills are rarely precise even without an
        intentional malicious behaviour from the workers. (I assume
        that authors are using self declared skills otherwise this
        assumption is meaningless). 
    3. Results: in the experiment results, the proposed weight
    measures does not seem to provide an enhancement over the existing
    weight measures (for both time and quality) 
    4. In the state of the art : slightly weak state of the art. Key
    references such as “Preserving worker privacy in crowdsourcing” of
    Kajino et al. is not mentioned. 
    5. minors : Authors say “Workers need a secure way to fetch their
    own assignment. This can be solved easily by well-known technical
    means. For example, the platform could post each assignment to a
    URI that would consist in the secure hash of the worker’s
    perturbed profile. Each worker would then be able to access this
    URI based on a secure web browsing (e.g., TOR14 ).”  This idea is
    not clear. However, authors should not that using TOR is usually
    forbidden by the terms and conditions of crowdsourcing systems.
