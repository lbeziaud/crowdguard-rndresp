(TeX-add-style-hook "approach"
 (lambda ()
    (LaTeX-add-labels
     "sec:contrib"
     "fig:prob_eps"
     "claim:prflip"
     "alg:flip"
     "claim:flip-dp"
     "alg:match"
     "def:awf"
     "def:mwf"
     "eq:mwf"
     "def:cwf"
     "eq:n-wf"
     "def:twf"
     "eq:twf"
     "sec:post")))

