(TeX-add-style-hook "problem"
 (lambda ()
    (LaTeX-add-labels
     "sec:prelim"
     "fig:taxonomy"
     "sec:cs"
     "sec:qualsec"
     "th:dpcomp"
     "def:qualrel"
     "def:fpa")))

