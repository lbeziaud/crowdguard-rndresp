(TeX-add-style-hook "report-dexa17"
 (lambda ()
    (LaTeX-add-bibliographies
     "crowdguard-rndresp-VLIGHT")
    (LaTeX-add-environments
     "algorithm")
    (TeX-add-symbols
     '("dga" 1)
     '("ta" 1)
     '("lb" 1)
     '("notr" 1)
     '("tr" 1)
     "centerfloat")
    (TeX-run-style-hooks
     "tikz"
     "pgfplots"
     "algpseudocode"
     "noend"
     "amssymb"
     "amsfonts"
     "amsmath"
     "mdwlist"
     "lmodern"
     "fontenc"
     "T1"
     "inputenc"
     "utf8"
     "graphicx"
     "final"
     "wrapfig"
     "subfig"
     "booktabs"
     "enumitem"
     "algorithm2e"
     "linesnumbered"
     "vlined"
     "ruled"
     "xcolor"
     "ifthen"
     "ifdraft"
     "latex2e"
     "llncs10"
     "llncs"
     ""
     "intro"
     "problem"
     "approach"
     "evaluation"
     "related"
     "conclusion")))

