import pandas as pd
import numpy as np
import seaborn as sns
import math
import matplotlib.pyplot as plt
import networkx as nx
import itertools
import json
import numpy as np
from networkx.readwrite import json_graph
sns.set(rc={'text.usetex' : True})
sns.set_style('whitegrid')
sns.set_context('paper')
sns.set_palette('cubehelix')
sns.set_context('paper', font_scale=1.4)

# for r, h in itertools.product([3, 4], [3, 4]):
#     t = nx.balanced_tree(r, h, create_using=nx.DiGraph())
#     skills = [x for x in t.nodes_iter() if t.out_degree(x) == 0 and t.in_degree(x) == 1]
#     print(r, h, len(skills))
# 3 3 27
# 3 4 81
# 4 3 64
# 4 4 256

fn = '../taxonomy/skill-project/taxonomy.json'
with open(fn) as f:
    data = json.load(f)
t = json_graph.tree_graph(data)
nx.relabel_nodes(t, {k: v for v, k in enumerate(nx.topological_sort(t))}, copy=False)

skills = [x for x in t.nodes_iter() if t.out_degree(x) == 0 and t.in_degree(x) == 1]

f, (ax1, ax2) = plt.subplots(1, 2)

f.subplots_adjust(wspace=.1)

ax1.set_title('out-degree among non-leaf nodes')
d = [d for n, d in t.degree(t).items() if n not in skills]
sns.distplot(d, color='black', ax=ax1)

ax2.yaxis.set_ticks_position('right')

ax2.set_title('depth among leaf nodes')
h = [nx.shortest_path_length(t, 0, s) for s in skills]
sns.distplot(h, color='black', ax=ax2)

sns.plt.savefig('../papier-dexa17/figs/skillproject.png', bbox_inches='tight', dpi=300)
