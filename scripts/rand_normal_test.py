import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math
sns.set(rc={'text.usetex' : True})
sns.set_style('whitegrid')
sns.set_context('paper')
sns.set_palette('cubehelix')
sns.set_context('paper', font_scale=1.4)

def rand_normal(m, r):
    theta = 2 * np.pi * np.random.random()
    mu = np.sqrt(r) * np.cos(theta)
    sigma = np.sqrt(r) * np.abs(np.sin(theta))
    # print(m, r, theta, mu, sigma)
    hist = np.clip(np.histogram(np.random.normal(mu, sigma, 1000), bins=np.linspace(-1, 1, num=m+1), normed=True)[0], 0, 1)
    return hist

# for _ in range(10):
#     r=0.005;
#     theta=2*np.pi*np.random.random();
#     mu=np.sqrt(r)*np.cos(theta);
#     sigma=np.sqrt(r)*np.abs(np.sin(theta));
#     h=np.clip(np.histogram(np.random.normal(mu, sigma, 1000), bins=np.linspace(-1, 1, num=10), normed=True)[0], 0, 1);
#     (h, h.mean())

m = 64
n = 1000
h = [0 for _ in range(m)]
number_skills = []
for _ in range(n):
    s = [np.random.random() < p for p in rand_normal(m, r=0.06)]
    h = [x + y for x, y in zip(h, s)]
    c = sum(s)
    number_skills.append(c)
    
f, (ax1, ax2) = plt.subplots(1, 2)

f.subplots_adjust(wspace=.1)

ax1.set_title('number of skills per profiles')
sns.distplot(number_skills, ax=ax1)

ax2.yaxis.set_ticks_position('right')

ax2.set_title('number of profiles skills')
# x = np.array(range(m))
# y = np.array(h) / n
# sns.barplot(x, y, color='black', ax=ax2)
sns.distplot(h, ax=ax2)

sns.plt.savefig('../papier-dexa17/figs/normal81.png', bbox_inches='tight', dpi=300)
