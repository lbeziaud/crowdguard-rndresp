import numpy as np
import networkx as nx
from collections import defaultdict
from networkx.readwrite import json_graph
from scipy.spatial import distance
from scipy.optimize import linear_sum_assignment

n = 50

r, h = 4, 3
st = nx.balanced_tree(r, h, create_using=nx.DiGraph())

# print(json_graph.tree_data(st, root=0))

# import matplotlib.pyplot as plt
# nx.draw(st, pos=nx.nx_pydot.pydot_layout(st, root=0))
# plt.show()

s = [x for x in st.nodes_iter() if st.out_degree(x) == 0 and st.in_degree(x) == 1]
st = st.to_undirected()

workers = [defaultdict(int) for _ in range(n)]
tasks = [defaultdict(int) for _ in range(n)]

for w in workers:
    for k in s:
        w[k] = np.random.randint(0, 2)
for t in tasks:
    for k in s:
        t[k] = np.random.randint(0, 2)
        
costs_ham = np.empty((n, n))
costs_nbg = np.empty((n, n))
costs_cos = np.empty((n, n))
for i, w in enumerate(workers):
    w2 = {x: w[x] + sum(w[y] / nx.shortest_path_length(st, source=x, target=y) for y in s if y != x) for x in s}
    print(w, w2)
    
    for j, t in enumerate(tasks):
        costs_ham[i, j] = sum(t[k] and not w[k] for k in s) / len(s)

        # c = 0
        # for k in s:
        #     if t[k]:
        #         c += w[k]
        #         for x in s:
        #             if k != x:
        #                 c += 1 / nx.shortest_path_length(st,source=k,target=x)        
        #w2 = {si: w[i] + sum(t[j] / max(1, nx.shortest_path_length(st, source=si, target=sj)) for j, sj in enumerate(s)) for i, si in enumerate(s)}
        #c = sum(w2[k] for k in s if t[k])
        costs_nbg[i, j] = sum(w2[x] for x in s if t[x]) # c / len(s)

        if any(t[k] != 0 for k in s) and any(w[k] != 0 for k in s):
            costs_cos[i, j] = distance.cosine([t[k] for k in s], [w[k] for k in s])

        print(i, j)

costs_ham /= costs_ham.max()
costs_nbg /= costs_nbg.max()
costs_cos /= costs_cos.max()

costs_nbg *= -1
costs_nbg += 1
        
# print(costs_ham)
# print(costs_nbg)
# print(costs_cos)


import seaborn as sns
import matplotlib.pyplot as plt

f, axes = plt.subplots(1, 3, figsize=(18, 6), sharex=True, sharey=True)

sns.heatmap(costs_ham, ax=axes[0])
sns.heatmap(costs_nbg, ax=axes[1])
sns.heatmap(costs_cos, ax=axes[2])

plt.show()


a_ham = linear_sum_assignment(costs_ham)
a_nbg = linear_sum_assignment(costs_nbg)
a_cos = linear_sum_assignment(costs_cos)

print(costs_ham[a_ham].sum() / n)
print(costs_ham[a_nbg].sum() / n)
print(costs_ham[a_cos].sum() / n)

print(costs_nbg[a_ham].sum() / n)
print(costs_nbg[a_nbg].sum() / n)
print(costs_nbg[a_cos].sum() / n)

print(costs_cos[a_ham].sum() / n)
print(costs_cos[a_nbg].sum() / n)
print(costs_cos[a_cos].sum() / n)
