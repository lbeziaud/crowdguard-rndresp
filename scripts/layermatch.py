from functools import partial
import copy
import multiprocessing as mp
import json
from networkx.readwrite import json_graph
import networkx as nx
from random import random
from collections import defaultdict
import numpy as np
from scipy.optimize import linear_sum_assignment
from scipy.spatial import distance
from itertools import groupby
from operator import itemgetter
from scipy.stats import entropy
from numpy.linalg import norm
import numpy as np
from collections import Counter

import warnings
warnings.filterwarnings('error')

def taxo_perfect(r, h):
    return nx.balanced_tree(r, h, create_using=nx.DiGraph())

def taxo_skillproject(fn='../taxonomy/skill-project/taxonomy.json'):
    with open(fn) as f:
        data = json.load(f)
    T = json_graph.tree_graph(data)
    nx.relabel_nodes(T, {k: v for v, k in enumerate(nx.topological_sort(T))}, copy=False)
    return T

def level_order(T):
    return [(l, list(map(itemgetter(0), g))) for l, g in
            groupby(sorted(nx.single_source_shortest_path_length(T, 0).items()), key=itemgetter(1))]

def leaves(T):
    return [x for x in T.nodes_iter() if T.out_degree(x) == 0 and T.in_degree(x) == 1]

def distrib_normal_tree(T):
    sdist = defaultdict(int)
    for parent, children in nx.dfs_successors(T, 0).items():
        if parent == 0: # root
            sdist[parent] = 1
        ps = np.random.normal(size=len(children))
        ps = ps / sum(ps)
        for child, p in zip(children, ps):
            sdist[child] = sdist[parent] * p
    return sdist

def distrib_uniform(p=0.1):
    return defaultdict(lambda: p)

def flip(X, S, theta, alpha=0.5):
    Xf = copy.deepcopy(X)
    for x in Xf:
        for s in S:
            if random() < theta:
                x[s] = x[s]
            else: # innocuous question
                if random() < alpha:
                    x[s] = True
                else:
                    x[s] = False
    return Xf

def group(X, T, S):
    descendants = {x: nx.descendants(T, x) for x in T}
    num_leaves = {x: sum(1 for y in S if y in descendants[x]) for x in T}
    Xt = []
    for x in X:
        xt = defaultdict(int)
        for s in S:
            xt[s] = x[s]
            for i in nx.shortest_path(T, 0, s)[:-1]:
                xt[i] += xt[s]
            for i in T:
                xt[i] /= max(1, num_leaves[i])
        Xt.append(xt)
    return Xt

def profiles(n, sdist, S):
    X = [defaultdict(int) for _ in range(n)]
    for x in X:
        for s in S:
            x[s] = np.random.random() < sdist[s]
    return X

def costs_mat(n, X, Y, distance):
    C = np.empty((n, n))
    for i, x in enumerate(X):
        for j, y in enumerate(Y):
            C[i][j] = distance(x, y)
    return C

def dist_layer(w, r, L, S, T):
    # h = len(L) - 1
    # c = 0
    # for l, sl in L:
    #     u = [w[s] for s in sl]
    #     v = [r[s] for s in sl]
    #     if l == h-1: # leaves
    #         # use the same distance as flipmatch for the leaves
    #         c += sum([r[s] and not w[s] for s in S]) # / len(S)
    #     else:
    #         # 2
    #         c += len(sl) * sum([abs(x - y) / max(1, max(x, y)) for x, y in zip(u, v)])
            
    #         # 1 normalized
    #         # c += (l/h) * sum([abs(x - y) / max(1, max(x, y)) for x, y in zip(u, v)]) / len(S)

    #         # 1 
    #         # c += l * sum([abs(x - y) / max(1, max(x, y)) for x, y in zip(u, v)])

    #         # 3
    #         # if any(s != 0 for s in u) and any(s != 0 for s in v): # non-zero
    #         #     c += (l/h) * distance.cosine(u, v)
    # return c
    w2 = {x: w[x] + sum(w[y] / nx.shortest_path_length(T, source=x, target=y) for y in S if y != x) for x in S}
    c = sum(w2[x] for x in S if r[x])
    return c

def all_matchings(costs_mats):
    results = map(linear_sum_assignment, costs_mats)
    return results

def all_costs(n, W, R, Wf, Wt, Rt, S, L, T):
    opti_costs = costs_mat(n, W, R, distance=lambda w,r: sum([r[s] and not w[s] for s in S]) / len(S))
    random_costs = np.random.random((n, n))
    flip_costs = costs_mat(n, Wf, R, distance=lambda w,r: sum([r[s] and not w[s] for s in S]))
    group_costs = costs_mat(n, Wt, Rt, distance=lambda w,r: dist_layer(w,r,L,S,T.to_undirected()))
    return [opti_costs, random_costs, flip_costs, group_costs]

def match_cost(match, costs, n):
    return costs[match].sum() / n

def simp_expe(T=None, n=100, theta=0.6, sdist=None):
    S = leaves(T) # skills (leaves)
    L = level_order(T) # nodes by level

    W = profiles(n, sdist, S) # workers
    R = profiles(n, sdist, S) # requesters

    Wf = flip(W, S, theta) # flipped workers
    Wt = group(Wf, T, S) # tree workers
    Rt = group(R, T, S) # tree requesters

    names = ['opti', 'random', 'flip', 'group']
    costs = all_costs(n, W, R, Wf, Wt, Rt, S, L, T)
    matchings = all_matchings(costs)
    costs = [match_cost(match, costs[0], n) for match in matchings]

    return costs
    

def full_expe(r=4, h=3, T=None, n=100, theta=0.6, sdist='uniform', pskill=0.1, repeat=10):
    # r branching factor
    # h height
    # n number workers/requesters
    # theta probability of true answer

    if T == None:
        T = taxo_perfect(r, h)
    S = leaves(T) # skills (leaves)
    L = level_order(T) # nodes by level
    if sdist == 'uniform': # skills distribution
        sdist = distrib_uniform(pskill) # uniform
    elif sdist == 'normal':
        sdist = distrib_normal_tree(T) # recursive normal

    avg_costs = None
    max_costs = None
    min_costs = None

    for _ in range(repeat):
        W = profiles(n, sdist, S) # workers
        R = profiles(n, sdist, S) # requesters

        Wf = flip(W, S, theta) # flipped workers
        Wt = group(Wf, T, S) # tree workers
        Rt = group(R, T, S) # tree requesters

        names = ['opti', 'random', 'flip', 'group']
        costs = all_costs(n, W, R, Wf, Wt, Rt, S, L)
        matchings = all_matchings(costs)
        costs = [match_cost(match, costs[0], n) for match in matchings]

        if not avg_costs:
            avg_costs = costs
            max_costs = costs
            min_costs = costs
        else:
            avg_costs = [sum(x) for x in zip(avg_costs, costs)]
            max_costs = [max(x) for x in zip(avg_costs, costs)]
            min_costs = [min(x) for x in zip(avg_costs, costs)]

    avg_costs = [x / repeat for x in avg_costs]
    
    return '{} {} {} {}\n'.format(theta, ' '.join(map(str, avg_costs)), ' '.join(map(str, max_costs)), ' '.join(map(str, min_costs)))    

if __name__ == '__main__':
    #r=4, h=3, T=None, n=100, theta=0.6, sdist='uniform', pskill=0.1

    T = taxo_perfect(r=4, h=3)
    sdist = distrib_normal_tree(T)
    costs = simp_expe(n=100, T=T, sdist=sdist, theta=0.6)
    for cost, name in zip(costs, ['opti', 'random', 'flip', 'group']):
        print('{} {}'.format(cost, name))
    
    # print('cost name num theta tree sdist')
    
    # for n in range(100, 1000+1, 100):
    #     for theta in np.linspace(0.5, 1, num=20):

    #         # T = taxo_skillproject()
    #         # sdist = distrib_normal_tree(T)
    #         # for _ in range(10):
    #         #         costs = simp_expe(n=n, T=T, sdist=sdist, theta=theta)
    #         #         for cost, name in zip(costs, ['opti', 'random', 'flip', 'group']):
    #         #             print('{} {} {} {} SkillProject TreeNormal'.format(cost, name, n, theta))

    #         # for pskill in [0.1, 0.2, 0.3, 0.4, 0.5]:
    #         #     sdist = distrib_uniform(pskill)
    #         #     for _ in range(10):
    #         #         costs = simp_expe(n=n, T=T, sdist=sdist, theta=theta)
    #         #         for cost, name in zip(costs, ['opti', 'random', 'flip', 'group']):
    #         #             print('{} {} {} {} SkillProject Uniform({})'.format(cost, name, n, theta, pskill))

    #         for r in range(2, 4+1):
    #             for h in range(2, 4+1):
    #                 T = taxo_perfect(r, h)
    #                 sdist = distrib_normal_tree(T)
    #                 for _ in range(10):
    #                     costs = simp_expe(n=n, T=T, sdist=sdist, theta=theta)
    #                     for cost, name in zip(costs, ['opti', 'random', 'flip', 'group']):
    #                         print('{} {} {} {} Perfect({},{}) TreeNormal'.format(cost, name, n, theta, r, h))

    #                 for pskill in [0.1, 0.2, 0.3, 0.4, 0.5]:
    #                     sdist = distrib_uniform(pskill)
    #                     for _ in range(10):
    #                         costs = simp_expe(n=n, T=T, sdist=sdist, theta=theta)
    #                         for cost, name in zip(costs, ['opti', 'random', 'flip', 'group']):
    #                             print('{} {} {} {} Perfect({},{}) Uniform({})'.format(cost, name, n, theta, r, h, pskill))

    # with open('expe1d3.dat', 'w') as f:
    #     f.write('theta cost method\n')
    #     f.flush()
    #     for theta in np.linspace(0.5, 1, num=10, endpoint=False):
    #         for i in range(20):
    #             costs = simp_expe(sdist='normal', theta=theta)
    #             for cost, name in zip(costs, ['opti', 'random', 'flip', 'group']):
    #                 f.write('{} {} {}\n'.format(theta, cost, name))
    #             f.flush()
    #         print(theta)
