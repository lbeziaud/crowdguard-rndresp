import timeit
import json
import numpy as np
import networkx as nx
from collections import defaultdict
from networkx.readwrite import json_graph
from scipy.spatial import distance
from scipy.optimize import linear_sum_assignment
from itertools import groupby
from operator import itemgetter
import copy
import itertools
from joblib import Parallel, delayed

class Taxonomy(object):
    def __init__(self, tree):
        self.tree = tree
        self.skills = [x for x in self.tree.nodes_iter() if self.tree.out_degree(x) == 0 and self.tree.in_degree(x) == 1]
        self.levels = [(l, list(map(itemgetter(0), g))) for l, g in groupby(sorted(nx.single_source_shortest_path_length(self.tree, 0).items()), key=itemgetter(1))]
        self.dmax = len(self.levels)
        self.descendants = {x: nx.descendants(self.tree, x) for x in self.tree}
        self.num_leaves = {x: sum(1 for y in self.skills if y in self.descendants[x]) for x in self.tree}
        utree = self.tree.to_undirected()
        self.sp = {s1: {s2: nx.shortest_path(utree, source=s1, target=s2) for s2 in self.tree} for s1 in self.tree}
        self.sp_len = {s1: {s2: nx.shortest_path_length(utree, source=s1, target=s2) for s2 in self.tree} for s1 in self.tree}
        self.lcas = {s1: {s2: None for s2 in self.skills} for s1 in self.skills}
        for s1 in self.skills:
            for s2 in self.skills:
                root_to_s1 = self.sp[0][s1]
                root_to_s2 = self.sp[0][s2]
                lca = 0
                for x1, x2 in zip(root_to_s1, root_to_s2):
                    if x1 == x2:
                        lca = x1
                    else:
                        break
                self.lcas[s1][s2] = lca

def wf_ham(x, y, taxonomy):
    return sum(y[s] == x[s] for s in taxonomy.skills)

def wf_rand(x, y, taxonomy):
    return np.random.random()

def MWF(x, y, taxonomy):
    return sum(y[s] and not x[s] for s in taxonomy.skills)

def TWF(x, y, taxonomy):
    a = sum(y[i] * x[i] for i in taxonomy.skills)
    if a != 0:
        d = sum(y[i] * x[j] * taxonomy.sp_len[i][j] for i in taxonomy.skills for j in taxonomy.skills) / a
    else:
        d = 0
    return d

def CWF(x, y, taxonomy):
    d = 0
    for i, skills_i in taxonomy.levels:
        u = [x[s] for s in skills_i]
        v = [y[s] for s in skills_i]
        d += (i * distance.cosine(u, v))
    return d

def PWF(x, y, taxonomy):
    def pwf(s1, s2):
        lca = taxonomy.lcas[s1][s2]
        depth_lca = taxonomy.sp_len[0][lca]
        dist = (taxonomy.dmax - depth_lca) / taxonomy.dmax
        return dist
    d = sum(min(pwf(s2, s1) for s2 in taxonomy.skills if y[s2]) for s1 in taxonomy.skills if x[s1])
    return d

weight_functions = {
    'hamming': wf_ham,
    'MWF': MWF,
    'TWF': TWF,
    'CWF': CWF,
    'rand': wf_rand,
    'PWF': PWF
}

taxonomy =  Taxonomy(nx.balanced_tree(r=3, h=4, create_using=nx.DiGraph()))

X = defaultdict(int)
Y = defaultdict(int)
Xt = defaultdict(int)
Yt = defaultdict(int)
for s in taxonomy.skills:
    X[s] = (np.random.random() < 0.5)
    Y[s] = (np.random.random() < 0.5)
    Xt[s] = X[s]
    Yt[s] = Y[s]
    for i in taxonomy.sp[0][s][:-1]:
        Xt[i] += Xt[s]
        Yt[i] += Yt[s]
        
for wf_name, wf in weight_functions.items():
    if wf_name == 'CWF':
        x = Xt
        y = Yt
    else:
        x = X
        y = Y

    t = timeit.timeit('wf(x, y, taxonomy)', number=1000, globals=globals())
    print(wf_name, t)
