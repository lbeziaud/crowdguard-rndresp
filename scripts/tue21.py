import json
import numpy as np
import networkx as nx
from collections import defaultdict
from networkx.readwrite import json_graph
from scipy.spatial import distance
from scipy.optimize import linear_sum_assignment
from itertools import groupby
from operator import itemgetter
import copy
import itertools

def MWF(x, y, skills):
    return sum(y[s] and not x[s] for s in skills)

def NWF(x, y, skills, sp_length): # TWF
    # x2 = {s1: x[s1] + sum(x[s2] / sp_length[s1][s2] for s2 in skills if s1 != s2) for s1 in skills}
    # d = sum(x[s] for s in skills if y[s])
    #
    # d = 0
    # for i in skills:
    #     for j in skills:
    #         d += x[i] * y[j] * 1/(sp_length[i][j] if i != j else 1)
    # d /= 1 # TODO
    # d /= sum((sp_length[i][j] if i != j else 1) for i in skills for j in skills) # normalize
    #
    # d = sum(y[i] * (x[i] * sum(x[j] * sp_length[i][j] for j in skills)) for i in skills)
    # d /= sum(sp_length[i][j] for i in skills for j in skills)
    # d = - d + 1
    #
    #d = sum(y[i] * (1 + sum(sp_length[i][j] for j in skills) - (x[i] + x[i] * sum(x[j] * sp_length[i][j] for j in skills)))/(1 + sum(sp_length[i][j] for j in skills)) for i in skills)
    #d /= len(skills)

    a = sum(y[i] * x[i] for i in skills)
    if a != 0:
        d = sum(y[i] * x[j] * sp_length[i][j] for i in skills for j in skills) / a
    else:
        d = 0
    return d

def LWF(x, y, levels): # CWF
    d = 0
    for i, skills_i in levels:
        u = [x[s] for s in skills_i]
        v = [y[s] for s in skills_i]
        if any(s != 0 for s in u) and any(s != 0 for s in v): # non-zero
            d += (i * distance.cosine(u, v))
    # d /= sum(i for i, _ in levels)
    return d

def skill_project():
    fn = '../taxonomy/skill-project/taxonomy.json'
    with open(fn) as f:
        data = json.load(f)
    t = json_graph.tree_graph(data)
    nx.relabel_nodes(t, {k: v for v, k in enumerate(nx.topological_sort(t))}, copy=False)
    return t

if __name__ == '__main__':
    print('taxo,distr,wf_eval,wf_match,n,pflip,qrel,qabs,qabs_p,cost,cost_p')
    
    repeat = 10
    n = 100
    alpha = 0.5

    taxonomies = [('Perfect{}{}'.format(r, h), nx.balanced_tree(r, h, create_using=nx.DiGraph())) for r, h in
                  itertools.product([3,4], [3,4])]
    taxonomies += [('SkillProject', skill_project())]
    for taxo_name, t in taxonomies:
    
        skills = [x for x in t.nodes_iter() if t.out_degree(x) == 0 and t.in_degree(x) == 1]
        levels = [(l, list(map(itemgetter(0), g))) for l, g in groupby(sorted(nx.single_source_shortest_path_length(t, 0).items()), key=itemgetter(1))]
        descendants = {x: nx.descendants(t, x) for x in t}
        num_leaves = {x: sum(1 for y in skills if y in descendants[x]) for x in t}
        t = t.to_undirected() # for the path lengths
        sp_length = {s1: {s2: nx.shortest_path_length(t, source=s1, target=s2) for s2 in skills} for s1 in skills}

        # distributions = [('Bern({})'.format(p), defaultdict(lambda: p)) for p in [0.1]]
        distr = defaultdict(int)
        for parent, children in nx.dfs_successors(t, 0).items():
            if parent == 0: # root
                distr[parent] = 1
            c_distr = np.random.normal(size=len(children))
            c_distr = c_distr / sum(c_distr)
            for child, c_p in zip(children, c_distr):
                distr[child] = distr[parent] * c_p
        distributions = [('Normal10', distr), ('Bernoulli1', defaultdict(lambda: 0.1))]

        for distr_name, distr in distributions:

            for n in [100]:
            
                for theta in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:

                    for _ in range(repeat):

                        X = [defaultdict(int) for _ in range(n)]
                        Xf = [defaultdict(int) for _ in range(n)]
                        Xt = [defaultdict(int) for _ in range(n)]
                        Xft = [defaultdict(int) for _ in range(n)]
                        Y = [defaultdict(int) for _ in range(n)]
                        Yt = [defaultdict(int) for _ in range(n)]

                        for x, xf, xt, xft, y, yt in zip(X, Xf, Xt, Xft, Y, Yt):
                            for s in skills:
                                # workers
                                x[s] = np.random.random() < distr[s]
                                # tasks
                                y[s] = np.random.random() < distr[s]

                                # flip
                                xf[s] = x[s] if np.random.random() < theta else np.random.random() < alpha

                                # group
                                xt[s] = x[s]
                                xft[s] = xf[s]
                                yt[s] = y[s]
                                for i in nx.shortest_path(t, 0, s)[:-1]:
                                    xt[i] += xt[s]
                                    xft[i] += xft[s]
                                    yt[i] += yt[s]
                                # no need to normalize since cosine doesnt care
                                # for i in t:
                                #     xt[i] /= max(1, num_leaves[i])
                                #     xft[i] /= max(1, num_leaves[i])

                        wf_names = ['MWF', 'TWF', 'CWF']
                        wfs = [MWF, NWF, LWF]

                        weights = {wf_name: np.empty((n, n)) for wf_name in wf_names}
                        weights_p = {wf_name: np.empty((n, n)) for wf_name in wf_names}

                        for i in range(n):
                            for j in range(n):
                                for wf_name, wf in zip(wf_names, wfs):
                                    if wf_name == 'MWF':
                                        weights[wf_name][i, j] = wf(X[i], Y[j], skills)
                                        weights_p[wf_name][i, j] = wf(Xf[i], Y[j], skills)
                                    elif wf_name == 'TWF':
                                        weights[wf_name][i, j] = wf(X[i], Y[j], skills, sp_length)
                                        weights_p[wf_name][i, j] = wf(Xf[i], Y[j], skills, sp_length)
                                    elif wf_name == 'CWF':
                                        weights[wf_name][i, j] = wf(Xt[i], Yt[j], levels)
                                        weights_p[wf_name][i, j] = wf(Xft[i], Yt[j], levels)

                        # # normalize
                        # for wf_name in wf_names:
                        #     weights[wf_name] /= weights[wf_name].max() if weights[wf_name].max() != 0 else 1
                        #     weights_p[wf_name] /= weights_p[wf_name].max() if weights_p[wf_name].max() != 0 else 1

                        # similarity to distance
                        # weights['NWF'] *=  -1
                        # weights_p['NWF'] *= -1
                        # weights['NWF'] +=  1
                        # weights_p['NWF'] += -1

                        assign = {wf_name: linear_sum_assignment(weights[wf_name]) for wf_name in wf_names}
                        assign_p = {wf_name: linear_sum_assignment(weights_p[wf_name]) for wf_name in wf_names}

                        # print(weights['MWF'][assign_p['MWF']].sum() / n,
                        #       weights['MWF'][assign_p['NWF']].sum() / n,
                        #       weights['MWF'][assign_p['LWF']].sum() / n,
                        #       weights['MWF'][assign['MWF']].sum() / n,
                        #       weights['MWF'][assign['NWF']].sum() / n,
                        #       weights['MWF'][assign['LWF']].sum() / n)

                        for wf_name_match in wf_names:
                            qabs = sum(all(Y[i][s] >= X[j][s] for s in skills) for i, j in zip(*assign[wf_name_match])) / n
                            qabs_p = sum(all(Y[i][s] >= X[j][s] for s in skills) for i, j in zip(*assign_p[wf_name_match])) / n
                            
                            for wf_name_eval in wf_names:
                                cost = weights[wf_name_eval][assign[wf_name_match]].sum() / n
                                cost_p = weights[wf_name_eval][assign_p[wf_name_match]].sum() / n
                                qrel = cost / cost_p # what if match is optimal?

                                print('"{}","{}","{}","{}",{},{},{},{},{},{},{}'
                                      .format(taxo_name, distr_name, wf_name_eval, wf_name_match,
                                              n, 1-theta,
                                              qrel, qabs, qabs_p, cost, cost_p))
