import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math
sns.set(style='whitegrid', rc={"lines.linewidth": 1, 'text.usetex' : True})

def force_labels(g):
    xlabels,ylabels = [],[]

    for ax in g.axes[-1,:]:
        xlabel = ax.xaxis.get_label_text()
        xlabels.append(xlabel)
    for ax in g.axes[:,0]:
        ylabel = ax.yaxis.get_label_text()
        ylabels.append(ylabel)

    for i in range(len(xlabels)):
        for j in range(len(ylabels)):
            g.axes[j,i].xaxis.set_label_text(xlabels[i])
            g.axes[j,i].yaxis.set_label_text(ylabels[j])

cols = {
    'taxo_name': 'taxo',
    'distr_name': 'distr',
    'wf_name': '$\mathtt{wf}$',
    'n': '$n$',
    'pr_flip': '$\Pr_{flip}$',
    'qrel': '$\mathtt{q_{rel}}$',
    'qabs': '$\mathtt{fpa}$',
    'qabs_p': '$\mathtt{\widetilde{fpa}}$',
    'cost': '$\sum\mathtt{C}$',
    'cost_p': '$\sum\mathtt{\widetilde{C}}$'
}

df = pd.read_csv('fri24.1.PWF.csv', sep=',', names=['taxo','distr','wf','n','prflip','qrel','qabs','qabsp','cost','costp'], skiprows=1) # tue21.15.csv

for taxo in df.taxo.unique():#['Perfect34']:
    for distr in df.distr.unique():#['Bernoulli01', 'Bernoulli1', 'Bernoulli3']:

        g = sns.FacetGrid(col='wf', data=df[(df.taxo == taxo) & (df.distr == distr)],
                          legend_out=True, sharex=True, sharey=False, margin_titles=False)
        g = g.map(sns.regplot, 'prflip', 'qrel', marker='x')
        g.set_axis_labels("$\Pr_{flip}$", "$\mathtt{q_{rel}}$")
        g.fig.subplots_adjust(wspace=0.1, hspace=0.3)
        force_labels(g)
        sns.plt.savefig('../papier-www-16/figs/fri24.1.PWF.{}.{}.qrel.png'.format(taxo, distr), bbox_inches='tight')
        
        g = sns.FacetGrid(col='wf', data=df[(df.taxo == taxo) & (df.distr == distr)],
                          legend_out=True, sharex=True, sharey=False, margin_titles=False)
        g = g.map(sns.regplot, 'prflip', 'qabs', label='perturbed', marker='x')
        g = g.map(sns.regplot, 'prflip', 'qabsp', marker='+', color='g', label='original', scatter=True, ci=1, line_kws={'linestyle':'--'})
        g.set_axis_labels("$\Pr_{flip}$", "$\mathtt{fpa}$")
        g.fig.subplots_adjust(wspace=0.1, hspace=0.3)
        force_labels(g)
        sns.plt.legend(loc=9, bbox_to_anchor=(-0.6, -0.2), ncol=2)
        sns.plt.savefig('../papier-www-16/figs/fri24.1.PWF.{}.{}.qabs.png'.format(taxo, distr), bbox_inches='tight')

        # g = sns.FacetGrid(col='wf', data=df[(df.taxo == taxo) & (df.distr == distr)],
        #                   legend_out=True, sharex=True, sharey=False, margin_titles=False)
        # g = g.map(sns.pointplot, 'prflip', '', marker='+', color='g', scatter=True, ci=1, label='original', line_kws={'linestyle':'--'})
        # g = g.map(sns.pointplot, names[5], names[10], label='perturbed', marker='x')
        # g.set_axis_labels("$\Pr_{flip}$", "average assignment cost")
        # g.fig.subplots_adjust(wspace=0.1, hspace=0.3)
        # force_labels(g)
        # sns.plt.legend(loc=9, bbox_to_anchor=(-0.6, -0.2), ncol=2)
        # sns.plt.savefig('../papier-www-16/figs/tue21.15.{}.{}.cost.png'.format(taxo, distr), bbox_inches='tight')
