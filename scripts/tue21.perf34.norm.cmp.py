import json
import numpy as np
import networkx as nx
from collections import defaultdict
from networkx.readwrite import json_graph
from scipy.spatial import distance
from scipy.optimize import linear_sum_assignment
from itertools import groupby
from operator import itemgetter
import copy
import itertools
from joblib import Parallel, delayed

def wf_ham(x, y, taxonomy):
    return sum(y[s] == x[s] for s in taxonomy.skills)

def wf_rand(x, y, taxonomy):
    return np.random.random()

def MWF(x, y, taxonomy):
    return sum(y[s] and not x[s] for s in taxonomy.skills)

def TWF(x, y, taxonomy):
    a = sum(y[i] * x[i] for i in taxonomy.skills)
    if a != 0:
        d = sum(y[i] * x[j] * taxonomy.sp_len[i][j] for i in taxonomy.skills for j in taxonomy.skills) / a
    else:
        d = 0
    return d

def CWF(x, y, taxonomy):
    d = 0
    for i, skills_i in taxonomy.levels:
        u = [x[s] for s in skills_i]
        v = [y[s] for s in skills_i]
        d += (i * distance.cosine(u, v))
    return d

def PWF(x, y, taxonomy):
    def pwf(s1, s2):
        lca = taxonomy.lcas[s1][s2]
        depth_lca = taxonomy.sp_len[0][lca]
        dist = (taxonomy.dmax - depth_lca) / taxonomy.dmax
        return dist
    d = sum(min(pwf(s2, s1) for s2 in taxonomy.skills if y[s2]) for s1 in taxonomy.skills if x[s1])
    return d

def rand_normal(m, r):
    theta = 2 * np.pi * np.random.random()
    mu = np.sqrt(r) * np.cos(theta)
    sigma = np.sqrt(r) * np.abs(np.sin(theta))
    hist = np.clip(np.histogram(np.random.normal(mu, sigma, 1000), bins=np.linspace(-1, 1, num=m+1), normed=True)[0], 0, 1)
    return hist

class Taxonomy(object):
    def __init__(self, tree):
        self.tree = tree
        self.skills = [x for x in self.tree.nodes_iter() if self.tree.out_degree(x) == 0 and self.tree.in_degree(x) == 1]
        self.levels = [(l, list(map(itemgetter(0), g))) for l, g in groupby(sorted(nx.single_source_shortest_path_length(self.tree, 0).items()), key=itemgetter(1))]
        self.dmax = len(self.levels)
        self.descendants = {x: nx.descendants(self.tree, x) for x in self.tree}
        self.num_leaves = {x: sum(1 for y in self.skills if y in self.descendants[x]) for x in self.tree}
        utree = self.tree.to_undirected()
        self.sp = {s1: {s2: nx.shortest_path(utree, source=s1, target=s2) for s2 in self.tree} for s1 in self.tree}
        self.sp_len = {s1: {s2: nx.shortest_path_length(utree, source=s1, target=s2) for s2 in self.tree} for s1 in self.tree}
        self.lcas = {s1: {s2: None for s2 in self.skills} for s1 in self.skills}
        for s1 in self.skills:
            for s2 in self.skills:
                root_to_s1 = self.sp[0][s1]
                root_to_s2 = self.sp[0][s2]
                lca = 0
                for x1, x2 in zip(root_to_s1, root_to_s2):
                    if x1 == x2:
                        lca = x1
                    else:
                        break
                self.lcas[s1][s2] = lca

class Dataset(object):
    def __init__(self, taxonomy, distribution, pr_flip, alpha=0.5, n=100):
        X = [defaultdict(int) for _ in range(n)]
        Xf = [defaultdict(int) for _ in range(n)]
        Xt = [defaultdict(int) for _ in range(n)]
        Xft = [defaultdict(int) for _ in range(n)]
        Y = [defaultdict(int) for _ in range(n)]
        Yt = [defaultdict(int) for _ in range(n)]
        for x, xf, xt, xft, y, yt in zip(X, Xf, Xt, Xft, Y, Yt):
            rand_s1 = np.random.choice(taxonomy.skills)
            rand_s2 = np.random.choice(taxonomy.skills)
            if distribution == 'Normal':
                m = len(taxonomy.skills)
                distribution = {s: np.random.random() < p
                                for s, p in zip(taxonomy.skills,
                                                rand_normal(m, r=0.06))}
            for s in taxonomy.skills:
                x[s] = (np.random.random() < distribution[s]) or (s == rand_s1)
                y[s] = (np.random.random() < distribution[s]) or (s == rand_s2)
                if np.random.random() >= pr_flip:
                    xf[s] = x[s]
                else:
                    xf[s] = np.random.random() < alpha
                xt[s] = x[s]
                xft[s] = xf[s]
                yt[s] = y[s]
                for i in taxonomy.sp[0][s][:-1]:
                    xt[i] += xt[s]
                    xft[i] += xft[s]
                    yt[i] += yt[s]
        self.X = X
        self.Xf = Xf
        self.Xt = Xt
        self.Xft = Xft
        self.Y = Y
        self.Yt = Yt
        self.n = n
        self.pr_flip = pr_flip
        self.alpha = alpha
        
taxonomies = {
    'Perfect34': Taxonomy(nx.balanced_tree(r=3, h=4, create_using=nx.DiGraph())),
}

skills_distrs = {
    'Normal': 'Normal',
}

weight_functions = {
    'hamming': wf_ham,
    'MWF': MWF,
    'TWF': TWF,
    'CWF': CWF,
    'rand': wf_rand,
    'PWF': PWF
}

def expe(taxo_name, distr_name, wf_name, datasets, pr_flip, base_weights):
    taxonomy = taxonomies[taxo_name]
    wf = weight_functions[wf_name]

    for dataset in datasets:
        n = dataset.n
        weights = np.empty((n, n))
        weights_p = np.empty((n, n))
        for i in range(n):
            for j in range(n):
                if wf_name == 'CWF':
                    x = dataset.Xt[i]
                    xf = dataset.Xft[i]
                    y = dataset.Yt[j]
                else:
                    x = dataset.X[i]
                    xf = dataset.Xf[i]
                    y = dataset.Y[j]

                weights[i, j] = wf(x, y, taxonomy)
                weights_p[i, j] = wf(xf, y, taxonomy)

        assign = linear_sum_assignment(weights)
        assign_p = linear_sum_assignment(weights_p)

        cost = base_weights[assign].sum()
        cost_p = base_weights[assign_p].sum()
        if cost_p != 0:
            qrel = cost / cost_p
        else:
            qrel = cost

        print('{},{},{},{},{},{},{},{}'.format(taxo_name, distr_name, wf_name, n, pr_flip, qrel, cost, cost_p))

    return


if __name__ == '__main__':
    repeat = 3
    jobs = 4

    print('taxo,distr,wf,n,prflip,qrel,cost,costp')

    configs = []
    
    for taxo_name, taxonomy in taxonomies.items():
        for distr_name, distribution in skills_distrs.items():
            for pr_flip in [0, 0.1, 0.2, 0.3, 0.4, 0.5]:
                datasets = [Dataset(taxonomy, distribution, pr_flip, n=100) for _ in range(repeat)]
                for dataset in datasets:
                    base_weights = np.empty((dataset.n, dataset.n))
                    for i in range(dataset.n):
                        for j in range(dataset.n):
                            base_weights[i, j] = PWF(dataset.X[i], dataset.Y[j], taxonomy)
                    for wf_name in weight_functions:
                        configs.append((taxo_name, distr_name, wf_name, datasets, pr_flip, base_weights))
    
    Parallel(n_jobs=jobs)(delayed(expe)(taxo_name, distr_name, wf_name, datasets, pr_flip, base_weights) for taxo_name, distr_name, wf_name, datasets, pr_flip, base_weights in configs)
