import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math
sns.set(rc={'text.usetex' : True})
sns.set_style('whitegrid')
sns.set_context('paper')
sns.set_context('paper', font_scale=1.2)
sns.set_palette('cubehelix')

def force_labels(g):
    xlabels,ylabels = [],[]

    for ax in g.axes[-1,:]:
        xlabel = ax.xaxis.get_label_text()
        xlabels.append(xlabel)
    for ax in g.axes[:,0]:
        ylabel = ax.yaxis.get_label_text()
        ylabels.append(ylabel)

    for i in range(len(xlabels)):
        for j in range(len(ylabels)):
            g.axes[j,i].xaxis.set_label_text(xlabels[i])
            g.axes[j,i].yaxis.set_label_text(ylabels[j])

def show_all(g):
    for ax in g.axes.flat:
        _ = plt.setp(ax.get_yticklabels(), visible=True)
        _ = plt.setp(ax.get_xticklabels(), visible=True)
            
cols = {
    'taxo': 'taxo',
    'distr': 'distr',
    'wf': '$\mathtt{wf}$',
    'n': '$n$',
    'prflip': '$\Pr_{flip}$',
    'qrel': '$\mathtt{q_{rel}}$',
    'qabs': '$\mathtt{fpa}$',
    'qabsp': '$\mathtt{\widetilde{fpa}}$',
    'cost': '$\sum\mathtt{C}$',
    'costp': '$\sum\mathtt{\widetilde{C}}$'
}

df = pd.read_csv('fri24_sp_light.csv', sep=',', names=['taxo','distr','wf','n','prflip','qrel','qabs','qabsp','cost','costp'], skiprows=1) # tue21.15.csv
#df = df[(df.distr != 'Bernoulli01') & (df.distr != 'Bernoulli1')]

def meanplot(x, y, **kwargs):
    v = y.mean()
    plt.axhline(y=v, **kwargs)


    
g = sns.FacetGrid(hue='wf', data=df, sharex=True, sharey=False, palette='muted', hue_kws={'markers': ['x', 'o', 'v', '^', 'D', 's']})
g = g.map(sns.pointplot, 'prflip', 'qrel', ci=None, join=True)
g.set_titles("$\mathtt{{ {col_name} }}$")
sns.plt.legend(['AWF','MWF', 'CWF', 'Hamming', 'Random', 'TWF'], loc=6, bbox_to_anchor=(1, 0.5))
g.set_axis_labels("$\Pr_{flip}$", "$\mathtt{q_{rel}}$")
sns.plt.savefig('../papier-www-16/figs/fig7.qrel.png', bbox_inches='tight', dpi=300)


g = sns.FacetGrid(hue='wf', data=df, sharex=True, sharey=False, palette='muted', hue_kws={'markers': ['x', 'o', 'v', '^', 'D', 's']})
g = g.map(sns.pointplot, 'prflip', 'qabsp', ci=None, join=True)
#g = g.map(sns.pointplot, 'prflip', 'qabs', ci=None, join=True, linestyles='--')
g.set_titles("$\mathtt{{ {col_name} }}$")
sns.plt.legend(['AWF','MWF', 'CWF', 'Hamming', 'Random', 'TWF'], loc=6, bbox_to_anchor=(1, 0.5))
g.set_axis_labels("$\Pr_{flip}$", "$\mathtt{fpa}$")
sns.plt.savefig('../papier-www-16/figs/fig7.qabs.png', bbox_inches='tight', dpi=300)
    

# g = sns.FacetGrid(col='wf', data=df,
#                   legend_out=True, sharex=False, sharey=False, margin_titles=False, col_wrap=3)
# g = g.map(sns.pointplot, 'prflip', 'qrel')
# g.set_axis_labels("$\Pr_{flip}$", "$\mathtt{q_{rel}}$")
# show_all(g)
# sns.plt.savefig('../papier-www-16/figs/fri24.spl.norm.qrel.png', bbox_inches='tight')
    
# g = sns.FacetGrid(col='wf', data=df,
#                   legend_out=True, sharex=False, sharey=False, margin_titles=False, col_wrap=3)
# g = g.map(sns.pointplot, 'prflip', 'qabsp', label='perturbed')
# g.map(meanplot, 'prflip', 'qabs', label='original', ls='dashed')
# g.set_axis_labels("$\Pr_{flip}$", "$\mathtt{fpa}$")
# sns.plt.legend()#loc=9, bbox_to_anchor=(-0.6, -0.2), ncol=2)
# show_all(g)
# sns.plt.savefig('../papier-www-16/figs/fri24.spl.norm.qabs.png', bbox_inches='tight')

