\vspace{-5pt}
\section{Problem Definition}%
\label{sec:prelim}%
\vspace{-8pt}

\subsubsection{Skills and Participants}

% We model below the three types of participants that interact together
% during a crowdsourcing process as well as the information
% (\emph{i.e.,} skills) based on which the interaction takes place (see
% Fig.~\ref{fig:intro-archi}).

%\vspace{-15pt}%
%\subsubsection{Skills Model} %
The set of skills that can be possessed by a worker (\emph{resp.}
requested by a task) is denoted $\mathcal{S}$. A worker's profile $p_i
\in \mathcal{P}$ (\emph{resp.}  a task $t_i \in \mathcal{T}$) is
represented by a bit vector, \emph{i.e.,} $p_i = {\{0,
  1\}}^{|\mathcal{S}|}$, where each bit corresponds to a skill $s_j
\in \mathcal{S}$ and is set to $1$ if the given worker has the given
skill (\emph{resp.}  the given task $t_i = {\{0, 1\}}^{|\mathcal{S}|}$
requests the given skill). %
%
Without loss of generality, we consider that each requester has a
single task and that the number of workers and requesters is the same
(\emph{i.e.,} $|\mathcal{P}|=|\mathcal{T}|$). %
%
Furthermore, we assume that a skills taxonomy $\mathcal{S}_T$
exists\footnote{In practice, skills taxonomies concerning numerous
  real-life contexts exist today (see, \emph{e.g.,} the Skill-Project
  \url{http://en.skill-project.org/skills/}, or Wand's taxonomies
  \url{http://www.wandinc.com/wand-skills-taxonomy.aspx}).}%
\cite{MavridisGross-AmblardMiklos2016}, structuring the skills
according to their semantic proximity, and is such that the skills in
$\mathcal{S}$ are the leaves of $\mathcal{S}_T$ (\emph{i.e.,} no
non-leaf node can be possessed nor requested). The non-leaf nodes of
the taxonomy are called \emph{super-skills}. %
% For simplicity, we omit here other kind of information (\emph{e.g.,}
% personal preferences, location, age) but we emphasize that our
% techniques can be extended easily to support a wide range of
% information. %

\begin{figure}[h]
  \centering \resizebox{0.8\linewidth}{!}{%
    \begin{tikzpicture}[ taxo/.style={ shape=rectangle, rounded
        corners, draw, align=center, top color=white, bottom
        color=black!10 }, auto, level 1/.style={sibling distance=9em},
      level 2/.style={sibling distance=5em} ]
      \node[taxo] {$\perp$} child { node[taxo] (Sport) {Sport} child {
          node[taxo] (Yoga) {Yoga} child { node[taxo] (YogaH) {Hata
              Yoga}} child { node[taxo] (YogaO) {\ldots}} } child {
          node[taxo] (SportO) {\ldots}
          % child { node[taxo] (BoxingO) {\ldots}}
        } } child { node[taxo] (CS) {CS} child { node[taxo] (Enc)
          {Crypto.}  child { node[taxo] (EncFHE) {FHE} } child {
            node[taxo] (EncO) {\ldots} } } child { node[taxo] (CSO)
          {\ldots}} } child { node[taxo] (H) {Health} child {
          node[taxo] (HAM) {Alt.}  child { node [taxo] (HAMO) {\ldots}
          } child { node [taxo] (HAMC) {Chemo. relief} } } child {
          node[taxo] (HO) {\ldots} } }; \node[align=left,anchor=west]
      at (-6, 0) (superskills) {\textbf{super-}\textbf{skills}};
      \node[align=left,anchor=west] at (-6, -3.9) (skills)
      {\textbf{skills} \textbf{$\mathcal{S}$}};
      \node[draw,dashed,inner sep=0.1cm,fit={(skills) (YogaH) (YogaO)
        (EncFHE) (EncO) (HAMO) ($(HAMC.east)+(2.2pt,0)$)}] (skills)
      {}; \node[draw,dashed,inner sep=0.1cm,fit=(superskills) (CSO)
      (Sport) (Yoga) (SportO) (CS) (Enc) (H) (HO) (HAM)] (superskills)
      {};
    \end{tikzpicture}
  }
  \caption{Example of a skill taxonomy $\mathcal{S}_T$}%
  \label{fig:taxonomy}
  \vspace{-10pt}%
\end{figure}

% \subsubsection{Workers and Requesters} %
% Both workers and requesters have storage, computing, and
% communication ressources (\emph{e.g.,} typical
% CPU/\-RAM/\-band\-wid\-th of today's personal computers); they
% differ from their privacy constraints. Indeed, each worker has a
% private profile, while each requester has non-confidential tasks.
% Without loss of generality, we consider that each requester has a
% single task and that the number of workers and requesters is the
% same (\emph{i.e.,} $|\mathcal{P}|=|\mathcal{T}|$). %
% \vspace{-15pt}%

%\subsubsection{Platform}
The platform is essentially in charge of intermediating between
workers and requesters. The workers' profiles are considered private
while the requesters' tasks are not. The platform holds the set of
workers' profiles, \emph{perturbed} to satisfy differential privacy
(defined below) and denoted $\widetilde{\mathcal{P}}$, as well as the
exact set of requesters' tasks $\mathcal{T}$. %
%\vspace{-15pt}%
%
%\subsubsection{Attack Model}%
All participants, \emph{i.e.,} workers, requesters, and the platform,
are considered to be \emph{honest-but-curious}. This means that they
participate in the protocol without deviating from its execution
sequence (\emph{e.g.,} no message tampering, no data forging) but they
will try to infer anything that is
compu\-ta\-tio\-nal\-ly-\-feas\-ible to infer about private data
(\emph{i.e.,} the set of workers' non-perturbed profiles
$\mathcal{P}$). %
\vspace{-5pt}%

\subsubsection{The Traditional Tasks-to-Workers Assignment Problem}%
\label{sec:cs}%

In a traditional context, where workers' profiles are not considered
private, the objective of the crowdsourcing platform is to assign a
worker to each task such that the overall expected quality is
maximized. This well-known combinatorial optimization problem is
referred as the assignment problem and can be expressed as a standard
linear problem~\cite{NAV:NAV3800020109} (assuming \(|\mathcal{T}| =
|\mathcal{P}|\)). %
% We define it formally as follows:
% \begin{problem}[Assignment Problem]
%   Given two sets, \(\mathcal{T}\) and \(\mathcal{P}\), of equal size,
%   and a weight function \(\mathtt{C}\colon \mathcal{T}\times
%   \mathcal{P}\to\mathbb{R}\), the assignment problem is to find a
%   bijection \(\mathtt{f}\colon \mathcal{P}\to \mathcal{T}\) such that
%   the cost function \(\sum_{t\in \mathcal{T}} \mathtt{C}
%   (t,\mathtt{f}(t))\) is minimized. %
% \end{problem}
Assignment algorithms rely on a %the %
\emph{weight function} \(\mathtt{C}\colon \mathcal{T}\times
\mathcal{P}\to\mathbb{R}\) in charge of defining the cost of each
assignment, \emph{i.e.,} the divergence between the requirements
vector of a task and the skills vector of a worker. %
Common weight functions include the usual distance metrics
(\emph{e.g.,} Hamming distance) or disimilarities (\emph{e.g.,} cosine
distance). %
% Numerous algorithms exist for solving the assignment
% problem~\cite{bertsekas1981new,NAV:NAV3800020109,MavridisGross-AmblardMiklos2016}.
Since our approach is independent from the algorithm, we simply use
the \textsc{Hungarian} method~\cite{NAV:NAV3800020109}, a standard
academic choice. %
% The \textsc{Hungarian} method computes an optimal
% matching in \(\mathcal{O}(n^3)\) running time. In a nutshell, it sets
% up all of the costs in a matrix, and uses a combination of row
% operations and zero covers to come up with a %$0$-cost%
% min-cost assignment, resulting in an optimal solution. We refer the
% interested reader to the original paper~\cite{NAV:NAV3800020109} for
% further details.

% \subsubsection{Security and Quality}%
% \label{sec:qualsec}%

%
\vspace{-5pt}%
%
\subsubsection{Security} %
We say that our approach is secure against honest-but-curious
participants if and only if no participant learns information about
the set of non-perturbed profiles $\mathcal{P}$ that has not been
perturbed by a differentially-private mechanism, where differential
privacy~\cite{DBLP:conf/icalp/Dwork06} - the current \emph{de facto}
standard model for disclosing personal information while satisfying
sound privacy guarantees - is defined below. %
%
% Differential privacy~\cite{DBLP:conf/icalp/Dwork06} is the current
% \emph{de facto} standard model for disclosing personal information
% while satisfying sound privacy guarantees. %
% Informally speaking, the differential privacy model requires that
% the outputs of a differentially-private algorithm be almost
% insensitive to the participation of any possible individual in the
% dataset input. %
Differential privacy is
self-composable~\cite{DBLP:conf/sigmod/McSherry09} and secure under
post-processing~\cite{DworkRoth2014}.


\begin{definition}[Differential Privacy~\cite{DBLP:conf/icalp/Dwork06}]
  A randomized mechanism \(\mathtt{M}\) satisfies
  \(\epsilon\)-differential privacy with $\epsilon>0$ if for any
  possible set of workers' profiles \(\mathcal{P}\) and
  \(\mathcal{P}'\) such that $\mathcal{P}'$ is $\mathcal{P}$ with one
  additional profile (or one profile less), and any possible set of
  output \(O \subseteq Range(\mathtt{M})\),
  \begin{equation}
    \Pr[\mathtt{M}(\mathcal{P}) \in O] \leq 
    e^\epsilon \times \Pr[\mathtt{M}(\mathcal{P}') \in O] .
  \end{equation}
\end{definition}%

% \begin{theorem}[Compositions~\cite{DBLP:conf/sigmod/McSherry09}]%
%   \label{th:dpcomp}
%   Let \(\mathtt{M}\) be a randomized mechanism which satisfies
%   \(\epsilon\)-differential privacy. Then: (1) executing
%   \(\mathtt{M}\) $i$ times sequentially over the dataset
%   \(\mathcal{D}\), each time with a privacy parameter set to
%   $\epsilon_i$, provides \((\sum_i \epsilon_i)\)-differential privacy
%   (sequential composition), and (2) executing \(\mathtt{M}\) over $i$
%   disjoint subsets of the dataset \(\mathcal{D}\), each time with a
%   privacy parameter set to $\epsilon_i$, provides \((\max_i
%   \epsilon_i)\)-differential privacy (parallel composition).
% \end{theorem}%


% \begin{theorem}[Post-Processing~\cite{DworkRoth2014}]
%   Let \(\mathtt{M}\) be a randomized mechanism which provides
%   \(\epsilon\)-differential privacy. Let \(\mathtt{f}\) be an
%   arbitrary randomized mapping. Then \(\mathtt{f} \circ \mathtt{M}\)
%   provides \(\epsilon\)-differential privacy.
% \end{theorem}

\vspace{-15pt}%

\subsubsection{Quality} %
The inherent information loss due to the differentially-private
perturbation impacts the quality of the worker-to-task
assignment. This is the price to pay to satisfy a sound privacy
model. We quantify this impact by measuring the relative increase of
the assignment cost as well as the fraction of profiles that have
\emph{all} the skills required by the task to which they are assigned
(see the full version for formal
definitions~\cite{beziaud-crowdrndresp-tr-17}).

% The \emph{relative quality} is the inverse of
% this increase:
% \begin{definition}[Relative Quality]%
%   \label{def:qualrel}%
%   Let $\widetilde{\mathcal{A}}$ (resp. $\mathcal{A}$) be the
%   assignment performed over the set of perturbed profiles
%   $\widetilde{\mathcal{P}}$ (resp.\ the set of non-perturbed profiles
%   $\mathcal{P}$). We define the relative quality as follows:
%    \begin{equation}
%      \mathtt{q_{rel}} %(\widetilde{\mathcal{A}}, \mathcal{A}) 
%      = 
%      \frac{\sum_{{(t,p) \in \mathcal{A}}} \mathtt{C} (t,p)}
%      {\sum_{{(t,\widetilde{p}) \in \widetilde{\mathcal{A}}}} 
%        \mathtt{C} (t,\widetilde{p})}
%    \end{equation}
% \end{definition}

% We complement the relative quality with the fraction of profiles that
% have \emph{all} the skills required by the task to which they are
% assigned:
% \begin{definition}[Fraction of Perfect Assignments]%
%   \label{def:fpa}%
%   Let $\widetilde{\mathcal{A}}$ be the assignment performed over the
%   set of perturbed profiles $\widetilde{\mathcal{P}}$. We define the
%   fraction of perturbed assignments as follows:
%    % \begin{equation}
%    %   \mathtt{fpa} %(\widetilde{\mathcal{A}}, \mathcal{A}) 
%    %   = 1/|\widetilde{A}| \times
%    %   \sum_{(t,\widetilde{p}) \in \widetilde{\mathcal{A}}} 
%    %   \left\lfloor 
%    %     \frac{\sum_{\forall i} t[i] \wedge p[i]}{\sum_{\forall i} t[i] \wedge t[i]} 
%    %   \right\rfloor 
%    % \end{equation}
%   \begin{equation}
%      \mathtt{fpa} %(\widetilde{\mathcal{A}}, \mathcal{A}) 
%      = 1/|\widetilde{A}| \times
%      \sum_{\forall i} (t[i]=1 \wedge p[i]=1) \vee t[i]=0
%    \end{equation}
% \end{definition}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "dexa17-short-CR"
%%% End:
