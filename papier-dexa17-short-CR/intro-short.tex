\section{Introduction}
\vspace{-8pt}

% Crowdsourcing et online on-demand work marketplaces
Crowdsourcing platforms are disrupting traditional work marketplaces.
Their ability to compute high-quality matchings between tasks and
workers, instantly and worldwide, for paid or voluntary work, has made
them unavoidable actors of the 21$^{st}$ century economy. Early
crowdsourcing platforms did not (and still do not) require strong and
specific skills; they include for example Amazon Mechanical
Turk\footnote{\url{https://www.mturk.com/}} (for online micro-tasks),
Uber\footnote{\url{https://www.uber.com/}} (for car-driving tasks), or
TaskRabbit\footnote{\url{https://www.taskrabbit.com/}} (for simple
home-related tasks---\emph{e.g.,} cleaning, repairing). Today's
crowdsourcing platforms now go one step further by addressing
skill-intensive contexts (\emph{e.g.,} general team
building\footnote{\url{https://tara.ai/}}, collaborative
engineering\footnote{\url{https://makake.co/}}) through the collection
and use of fine-grained worker profiles. Such platforms carry the
promise to facilitate, fasten, and spread innovation at an
unprecedented scale.

However abusive behaviors from crowdsourcing platforms against workers
are frequently reported in the news or on dedicated websites, whether
performed willingly or not (see, \emph{e.g.,} the privacy scandals due
to illegitimate accesses to the geolocation data of a well-known
drivers-riders company\footnote{\url{https://tinyurl.com/wp-priv}}, or
the large-scale exposure of workers' identifying and sensitive
information---\emph{e.g.,} real name, book reviews, or wish-list---%
through Amazon Mechanical Turk
IDs~\cite{LeaseHullmanBighamEtAl2013}). The problem is even more
pregnant with skill-intensive crowdsourcing platforms since they
collect detailed workers' profiles for computing highly accurate
matchings (\emph{e.g.,} demographics, encompassive set of skills,
detailed past experiences, personal preferences, daily availabilities,
tools possessed). %
% Even without considering fully the wealth of
% information in a worker's profile, a detailed set of skills together
% with the worker's location can already disclose significantly
% identifying and/or sensitive information (\emph{e.g.,} the only
% \texttt{expert in fully homomorphic encryption} with \texttt{advanced
%   knowledge in hatha yoga} from the \texttt{city of Rennes} also has a
% strong background in \texttt{alternative me\-di\-ci\-nes for
%   alleviating the side-effects of che\-mo\-the\-ra\-py} may reflect a
% health status---of the worker or of a relative---and be
% identifying). In some cases, even though a worker's profile does not
% contain any skill considered to be ``sensitive'', this is the act of
% participating to crowdsourced projects that is sensitive (\emph{e.g.,}
% a company may prefer that its employees \emph{do not participate} to
% crowdsourced projects in order to avoid any risk of disclosures). %
We advocate thus for a sound protection of workers' profiles against
illegitimate uses: in addition to the necessary compliance with
fundamental rights to privacy, it is a precondition for a wide
adoption of crowdsourcing platforms by individuals.

\begin{wrapfigure}{r}{0.5\linewidth}
  \vspace{-10pt} %
  \centering
  \includegraphics[width=0.82\linewidth]{introarchi.pdf}
  \caption{Our Approach to Privacy-Preserving Task Assignment}%
  \label{fig:intro-archi}
  \vspace{-12pt}
\end{wrapfigure}

Computing the assignment of tasks to workers is the foundamental role
of the platform (or at least facilitating it). This paper considers
precisely the problem of computing a high-quality matching between
skill-intensive tasks and workers while preserving workers'
privacy. To the best of our knowledge, this problem has only been
addressed by a single recent work~\cite{Kajino2016}. However, this
work is based on costly homomorphic encryption primitives which
strongly hamper its performances and prevent it to reason about skills
within the assignment algorithm (\emph{e.g.,} no use of semantic
proximity).

We propose an approach (see Fig.~1) that addresses these issues by
making the following contributions:
\begin{enumerate}[topsep=0pt]
\item A simple skills model for a worker's profile: a bit vector and a
  taxonomy.
\item An algorithm run independently by each worker for perturbing her
  profile locally before sending it to the platform. By building on
  the proven \emph{randomized response}
  mechanism~\cite{blair2015design,Warner1965}, this algorithm is
  privacy-preserving (provides sound \emph{differential privacy}
  guarantees~\cite{DworkRoth2014}), and lightweight (no
  cryptography, no distributed computation, only bitwise operations).
\item A suite of weight functions to be plugged in a traditional
  assignment algorithm run by the platform and dedicated to increase
  the quality of matchings performed over perturbed profiles. Our
  weight functions reduce the impact of the perturbation by leveraging
  the skills taxonomy, vertically and horizontally, averaging the
  skills according to their semantic proximity in order to reduce the
  variance of the differentially-private perturbation. The variance
  reduction is mathematically sound and does not jeopardize privacy.
\item An experimental study (see the full
  version~\cite{beziaud-crowdrndresp-tr-17}), over a synthetic
  taxonomy and various synthetic datasets, that shows promising
  preliminary results about the practical adequacy of our approach
  from the sides of performance and quality.
\end{enumerate}

For space reasons, we give in this paper an overview of our
approach. We refer the interested reader to the full version of our
work~\cite{beziaud-crowdrndresp-tr-17} that describes our approach in
details, presents its experimental results, and positions it with
respect to related work.

The rest of the paper is organized as
follows. Section~\ref{sec:prelim} introduces the notions used in our
approach and defines more precisely the problem we tackle. We overview
our algorithms in Section~\ref{sec:contrib} %
% and analyze them empirically in
% Section~\ref{sec:eval}. Section~\ref{sec:related} discusses related
% work.  Finally,
and conclude in Section~\ref{sec:disc} outlining interesting future
works.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "dexa17-short-CR"
%%% End: 
