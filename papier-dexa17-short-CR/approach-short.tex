\section{A Flip-based Approach}%
\label{sec:contrib}%
%
\vspace{-8pt}%
%

This section overviews our approach. We first focus on the workers'
side: we describe the algorithm that we propose for perturbing each
worker's profile, show its adequacy to our context, and demonstrate
that it complies with our security model. Second, we shift to the
platform's side. We explain how to reduce the impact of the
differentially private perturbation (while still satisfying
differential privacy) and we describe the assignment algorithm based
on perturbed profiles. Finally, we overview technical means for
letting workers fetch their assignment in a secure way in order to
complete it. We refer the interested reader to the full
version~\cite{beziaud-crowdrndresp-tr-17} for more details (including
the formal proofs).

\subsection{At a Worker's Side: Local Perturbation}%
%
\vspace{-5pt}%
%
\subsubsection{Building Block: Randomized Response}
% The randomized response mechanism is based on two basic operations,
% thus it does not produce additional cost. It is designed for binary
% values such as the possession of a skill. Furthermore, it is a
% client-side perturbation mechanism which allow the user (worker or
% tasker) to anonymize its data without trusting the platform. \lb{it
% could be implemented as a simple javascript routine runned at
% sign-up, before sending data.}

Randomized response~\cite{Warner1965} is a simple though powerful
perturbation mechanism shown to satisfy differential privacy (see
below). Basically, it inputs a single bit (\emph{e.g.,} the answer of
an individual to a sensitive boolean question) and flips it randomly
according to a well-chosen distribution probability. %
% We advocate its use in our approach for perturbing workers' profiles
% because it can be performed by each worker independently from the
% others without any need for cryptography nor network communication
% (beyond the necessary transfer of each perturbed profile to the
% platform). This would not be the case with other well-known
% differentially-private mechanisms, such as the addition of Laplace
% noise~\cite{DBLP:conf/icalp/Dwork06} for example, which require to
% aggregate data before perturbing it (usually by centralizing it).
%
% There exist multiple variants of the randomized response mechanism,
% some of them in the context of differential
% privacy~\cite{DworkRoth2014,Waseda2016}.
We describe below the variant called \emph{innocuous question} that we
use in this paper and show that it satisfies differential
privacy\footnote{Any other variant could have been used, provided that
  it satisfies differential privacy.}. Let \(x \in \{0,1\}\) be a
private value. The randomized response mechanism simply outputs the
perturbed value of $x$, denoted $\widetilde{x}$, as follows:

% Let \(Q_s\) be the sensitive question ``do you have skill \(x\)?,
% and \(Q_I\) be an innocuous (ie.\ unrelated) question which has a
% known probability \(\alpha\) of yielding a YES response.  With some
% (known) probability \(\theta\) a subject gets \(Q_s\), otherwise
% (with probability \(1 - \theta\)) he will answer \(Q_I\).

\[
\widetilde{x} = \begin{cases}
  x &\text{with probability } 1-\Pr_{flip} \\
  1 &\text{with probability } \Pr_{flip}\times\Pr_{inno} \\
  0 &\text{with probability } \Pr_{flip}\times(1-\Pr_{inno}) \\
\end{cases}
\] where %\(\Pr_{flip} < 0.5\) and
\(Pr_{flip}\) depends on $\epsilon$ (see below) and \(Pr_{inno} \in [0,
1]\). We use \(\Pr_{inno} = 0.5\) in the rest of the paper, since it
minimizes the variation of the estimated value of \(x\) after
perturbation~\cite{blair2015design}.

\begin{claim}%
  \label{claim:prflip}%
  For a given differential privacy parameter \(\epsilon>0\), and a
  worker's profile made of a single bit to be flipped, the innocuous
  question randomized response scheme satisfies
  \(\epsilon\)-differential privacy if \(\Pr_{flip} =
  \frac{2}{1+e^\epsilon}\) (see~\cite{beziaud-crowdrndresp-tr-17} for
  the proof).
\end{claim}
%
\vspace{-5pt}%
%

%
\vspace{-5pt}%
%
\subsubsection{Flip Mechanism} %
Our {\sc Flip} mechanism (Alg.~\ref{alg:flip}) essentially consists in
applying the randomized response mechanism to each binary skill of a
worker's profile before sending it to the platform and inherits thus
its high efficiency. The self-composability properties of differential
privacy allow that by distributing $\epsilon$ over the bits of the
skills vector.

\begin{algorithm}[tbhp]
  \SetKwInOut{Input}{Input}%
  \SetKw{Ret}{Return}%
  \SetKwFunction{RndResp}{\texttt{RandomizedResponse}}%
  \Input%
  {%
    %
    The original profile $p=\langle p[1], \ldots, p[l]\rangle$, the
    differential privacy budget $\epsilon>0$.
    % , the fraction of skills retained $\phi \in \lbrack 0,1\lbrack$.
    % 
  }%
  
  Let \texttt{Pr}$_{flip}$ be the flipping probability: $\Pr_{flip}
  \gets \frac{2}{1+e^{\epsilon/l}}$.  \\%
  % 
  
  Initiate the perturbed profile: $\widetilde{p} \gets \langle
  \widetilde{p}[1]=0, \dots, \widetilde{p}[l]=0 \rangle$. \\ %
  
  \For{$1\leq i\leq l$}%
  {%
    % $r \gets$ \emph{uniform random number in} $\lbrack 0,
    % 1\lbrack$.\\ %
    % %
    % \If{$r<\phi$} %
    % { %
      %
    $\widetilde{p}[i] \gets$ \RndResp{$p[i]$, $\Pr_{flip}$}. %
      % 
    % }%
  }
  
  %
  \Ret{The perturbed profile $\widetilde{p}$}
  \caption{\textsc{Flip} (run by each Worker)}%
  \label{alg:flip}%
\end{algorithm}%
%\vspace{-10pt}%

% \begin{algorithmic}[1]%
%   %   \Require $p<0.5$
%   \Function{Flip}{$\epsilon>0$, $p=\langle b_1, \ldots, b_k\rangle$,
%   $\phi \in [0,1[$}%
%   \State $\Pr_{flip} \gets \frac{1}{1+e^{\epsilon/(\phi \times
%   k)}}$ %
%   \State $\widetilde{p} \gets \langle \widetilde{b}_1=0, \dots,
%   \widetilde{b}_k=0 \rangle$%
%   \For{$i \in \{1, 2, \dots, k\}$}%
%   \State $r \gets$ \emph{uniform random number in $[0, 1[$} %
%   \If{$r<\phi$} %
%   \State $\widetilde{p}[i] \gets \Call{RndResp}{p[i], \Pr_{flip}}$ %
%   \EndIf
%   %   \State $s'_i\gets 1-s_i$%
%   %   \Else%
%   %   \State $s'_i\gets s_i$%
%   %   \EndIf%
%   \EndFor%
%   \State\Return $\widetilde{p}$%
%   \EndFunction%
% \end{algorithmic}%

\begin{claim}%
  \label{claim:flip-dp}%
  The \textsc{Flip} mechanism satisfies $\epsilon$-differential
  privacy (see~\cite{beziaud-crowdrndresp-tr-17} for the proof).
\end{claim}

\tr{
  \begin{proof}
    The proof follows the composition properties of differential
    privacy. First, let consider that we have a single worker with a
    skills vector $p$ of length $l$. We showed above that perturbing
    (and sending to the platform) a single bit of $p$ with a flipping
    probability $\Pr_{flip} = 2/(1+e^{\epsilon/l})$ satisfies
    $\frac{\epsilon}{l}$-differential privacy. It results from
    Theorem~\ref{th:dpcomp} that perturbing $l$ bits of $p$ all with
    probability $\Pr_{flip} = 2 / (1+e^{\epsilon/l})$ yields
    $(\sum_i^{l} \frac{\epsilon}{l})$-differential privacy,
    \emph{i.e.,} $\epsilon$-differential privacy. Hence, with a single
    worker, \emph{i.e.,} $|\widetilde{\mathcal{P}}|=1$, the
    \textsc{Flip} mechanism satisfies $\epsilon$-differential
    privacy. Let now consider an arbitrary number of workers, say
    $n$. Since each perturbed profile $p_i \in
    \widetilde{\mathcal{P}}$ consists in a disjoint subset of
    $\widetilde{\mathcal{P}}$, then it follows directly from
    Theorem~\ref{th:dpcomp} that the \textsc{Flip} mechanism applied
    in parallel by each worker satisfies $\max_i
    \epsilon_i$-differential privacy. Moreover, all workers use the
    same privacy budget $\epsilon_i = \epsilon$, so that $\max_i
    \epsilon_i=\epsilon$. As a result, the parallel execution of the
    \textsc{Flip} mechanism by the $n$ workers (\emph{i.e.,}
    $|\widetilde{\mathcal{P}}|=n$) satisfy $\epsilon$-differential
    privacy.
  \end{proof}
}


\subsection{At the Platform's Side: Task Assignment}%
%\subsection{FlipMatch}
%
\vspace{-5pt}%
%

\tr{
\begin{algorithm}[tbhp]
  \SetKwInOut{Input}{Input}%
  \SetKw{Ret}{Return}%
  \SetKwFunction{Hung}{\texttt{Hungarian}}%
  \Input%
  {%
    %
    The set of perturbed profiles $\widetilde{\mathcal{P}}$ (bit
    vectors), the set of tasks $\mathcal{T}$ (bit vectors), the weight
    function \(\mathtt{C}\colon \mathcal{T}\times
    \mathcal{P}\to\mathbb{R}\) to be used.
    % 
  } %
  
  % Let $\widetilde{\mathcal{A}}$ denote the final assignment. \\%
  % 
  
  Initiate the final assignments $\widetilde{\mathcal{A}}$. \\ %
  
  Perform the \textsc{Hungarian} method, based on \texttt{C}, for
  computing the task-to-worker assignment: $\widetilde{\mathcal{A}}
  \leftarrow$ \Hung{$\widetilde{\mathcal{P}}$, $\mathcal{T}$,
    $\mathtt{C}$}. \\ %
  %
  \Ret{The assignment $\widetilde{\mathcal{A}}$}
  \caption{\textsc{Match} (run by the Platform)}%
  % 
  \vspace{-3pt}%
  % 
  \label{alg:match}%
\end{algorithm}
}


Efficient traditional assignment algorithms do not need any
modification to work with perturbed profiles, which are bit vectors,
exactly as non-perturbed profiles are. %
The main question is the impact on quality due to our perturbation,
and this impact is naturally related to the weight function
\(\mathtt{C}\colon \mathcal{T}\times\mathcal{P}\to\mathbb{R}\) on
which assignment algorithms rely. %
As there is no clear consensus on what is a good weight function for
task assignment, in the sequel we recall several reasonable
functions, ignoring or using the skill taxonomy. %
We also propose new weight functions and explain how they could cope
with the diffentially-private perturbation.

%
\vspace{-5pt}%
%

\subsubsection{Existing Weight Functions}

Numerous weight functions have been proposed as metrics over
skills. The \emph{Hamming distance} is a common choice to compute
dissimilarity between two vectors of bits but it does not capture the
semantics needed for crowdsourcing (\emph{e.g.},\ a worker possessing
all the skills has a high Hamming distance from a task requiring only
one skill, although he is perfectly able to perform it). The weight
function proposed in~\cite{MavridisGross-AmblardMiklos2016} adresses
this problem based on a taxonomy. We slightly adapt it and call it the
\texttt{Ancestors} weight function (\texttt{AWF} for short).

%
\vspace{-5pt}%
%
\begin{definition}[\texttt{Ancestors} Weight Function (adapted from~\cite{MavridisGross-AmblardMiklos2016})]%
  \label{def:awf}%
  Let \(d_{max}\) be the maximum depth of the taxonomy
  \(\mathcal{S}^T\). Let \(\mathrm{lca}(s, s') \in \mathcal{S}^T\) be
  the lowest common ancestor of skills \(s\) and \(s'\) in the
  taxonomy.
  \begin{equation}
    \mathtt{AWF}(t, \widetilde{p}) = {
      \sum_{s_i \in \widetilde{p}} {
        \min_{s_j \in t} \left(
          \frac{d_{max} - \mathrm{depth}(\mathrm{lca}(s_i, s_j))}{d_{max}}
        \right)
      }
    }
   \end{equation}
\end{definition}
%
\vspace{-5pt}%
%

\subsubsection{Naive Skill-Level Weight Functions} %
The \texttt{Missing} weight function (\texttt{MWF} for short) between a
worker and a task revisits the Hamming distance. It settles a
task-to-worker assignment cost that is intuitive in a crowdsourcing
context: it is defined as the fraction of skills required by the task
that the worker does not have (see Definition~\ref{def:mwf}).

%
\vspace{-5pt}%
%
\begin{definition}[\texttt{Missing} Weight Function
  (\texttt{MWF})]%
  \label{def:mwf}
  \(\mathtt{MWF}\colon \mathcal{T}\times
  \widetilde{\mathcal{P}}\to\mathbb{R}\) is defined as follows:
  \begin{equation}
    \label{eq:mwf}
    \mathtt{MWF} (t, \widetilde{p}) = 
    \sum_{\forall i} t[i] \wedge \neg \widetilde{p}[i]
  \end{equation}%
  %
\end{definition}
%
\vspace{-5pt}%
%

\subsubsection{Leveraging the Taxonomy} %
In realistic contexts, the differentially private perturbation may
overwhelm the information contained in the original profiles and make
the perturbed profiles be close to uniformly random bit
vectors. % With
% a naive weight function, \textsc{Match} would assign tasks to random
% profiles.
We cope with this challenging issue by building on the taxonomy
$\mathcal{S}_\mathtt{T}$. Indeed, the taxonomy allows to group large
numbers of skills according to their semantic proximity and to reduce
the variance of the perturbation by using group averages% . Such a
% reduction is mathematically sound and formally assessed by the
% Bienaymé
% formula
~\cite{bienayme1853considerations}. % (see Theorem~\ref{th:bienayme}).
% Informally speaking, Bienaymé states that the variance of the mean
% of a set of uncorrelated random variables decreases linearly with
% the number of variables averaged. In our context this means that the
% more the number of perturbed skills averaged together, the less the
% magnitude of the differentially private perturbation (linear
% decrease).
%
% We propose two novel weight functions such that each leverages a
% precise type of information given by the taxonomy: the \emph{vertical
%   parent-to-children relationship} between skills, and the
% \emph{horizontal neighbouring proximity relationship}. %
% We stress that our two weight functions aim at illustrating simply the
% benefits of using these two relationships. %
% More complex alternative functions benefiting differently from the
% same information could be designed.

% \begin{theorem}[Bienaymé Formula~\cite{bienayme1853considerations}]%
%   \label{th:bienayme}%
%   Let $X_i$ be a set of uncorrelated random variables having the same
%   variance $\sigma^2$, then, the variance of their mean ${\overline
%     {X}}$ is:
%   \begin{equation}
%   % {\displaystyle \operatorname {Var} \left({\overline
%   %       {X}}\right)=\operatorname {Var} \left({\frac {1}{n}}\sum
%   %     _{i=1}^{n}X_{i}\right)={\frac {1}{n^{2}}}\sum
%   %   _{i=1}^{n}\operatorname {Var} \left(X_{i}\right)={\frac {\sigma
%   %       ^{2}}{n}}.} \\
%   % \operatorname {Var} \left({\overline {X}}\right) =
%   % \operatorname {Var} \left({\frac {1}{n}}\sum
%   %   _{i=1}^{n}X_{i}\right)={\frac {1}{n^{2}}}\sum
%   % _{i=1}^{n}\operatorname {Var} \left(X_{i}\right)={\frac {\sigma
%   %     ^{2}}{n}}.
% \operatorname {Var} \left({\overline
%         {X}}\right)=\operatorname {Var} \left({\frac {1}{n}}\sum
%       _{i=1}^{n}X_{i}\right)={\frac {\sigma
%       ^{2}}{n}}
%   \end{equation}%
% \end{theorem}%

%
\vspace{-5pt}%
%
\paragraph{Climbing Weight Function} %
The \texttt{Climbing} weight function (\texttt{CWF} for short)
leverages \emph{the vertical relationship} given by the taxonomy by
averaging, for each profile, the skills along the root-to-leaf
paths. In other words, before performing the assignment, the platform
converts each perturbed profile into a tree% \footnote{Converting the
% perturbed profiles in trees of scores can be performed in parallel
% by each worker and be sent to the platform together with her flipped
% profile.}
, \emph{i.e.,} the same as the taxonomy $\textsc{S}_T$, and for each
node $n$ of the tree, it computes the mean of the skills that appear
below $n$ (interpreting the boolean values $1$ and $0$ as
integers). For a given node, this mean is actually a rough estimator
of the fraction of descendant skills possessed. We call it
\emph{score} below. %
% It is worth to note that (1) the higher the node into the
% tree the more precise its score (Bienaymé Formula), and (2) usual
% statistical correction techniques can be applied to this estimator
% (\emph{e.g.,} a corrected estimator $\overline{\zeta}$ of $\zeta$
% can be computed as follows~\cite{ChenKiferLeFevreEtAl2009}:
% $\overline{\zeta}=\frac{\zeta-\Pr_{flip}}{1-2\Pr_{flip}}$).
During an assignment, given a task and a perturbed profile, the
\texttt{Climbing} weight function consists essentially in computing
the distance between the scores vector of the task and the scores
vector of the profile at each level. %
% , in weighting it by the level (the closer to the leaves the larger
% the weight), and finally in summing the whole. The variance
% reduction of the perturbation that \texttt{Climbing} is expected to
% benefit from is due to the average computed within each score. For
% each given score, the reduction is linearly proportional with the
% number of skills that it averages, which depends on the taxonomy
% used, but grows monotonically and inversely linearly with the
% level. %
Definition~\ref{def:cwf} formalizes the \texttt{Climbing} weight
function.

%
\vspace{-3pt}%
%
\begin{definition}[\texttt{Climbing} Weight Function
  (\texttt{CWF})]%
  \label{def:cwf}%
  Let $v_i$ (\emph{resp.} $u_i$) denote the scores vector at level $i$
  in the tree corresponding to the profile $\widetilde{p}$
  (\emph{resp.} to the task $t$), and $\mathtt{d} \colon
  \mathbb{R}^n\times \mathbb{R}^n\to\mathbb{R}$ be a classical
  distance function on real-valued vectors (\emph{e.g.,}
  \texttt{Cosine}). Then, \(\mathtt{CWF}\colon \mathcal{T}\times
  \widetilde{\mathcal{P}}\to\mathbb{R}\) is defined as follows:
  % $\leftrightarrow$
  \begin{equation}
    \label{eq:n-wf}
    \mathtt{CWF} (t, \widetilde{p}) = \sum_{\forall i} i \times \mathtt{d} (u_i, v_i)
  \end{equation}
\end{definition}
%
\vspace{-5pt}%
%

%
\vspace{-5pt}%
%
\paragraph{Touring Weight Function} %
The \texttt{Touring} weight function (\texttt{TWF} for short)
leverages \emph{the horizontal relationship} given by the taxonomy,
\emph{i.e.,} the neighbouring proximity degree between skills. As
described in Definition~\ref{def:twf}, it returns the average
path-length---according to the taxonomy $\mathcal{S}_T$---between the
skills required by the task and the skills of the worker's
profile. The expected variance reduction comes from the average
path-length of the full cartesian product between the skills required
by a task and the skills set to $1$ in a perturbed profile. The
reduction depends on the taxonomy (similarly to \texttt{Climbing}) and
on the number of skills averaged.

%
\vspace{-3pt}%
%
\begin{definition}[\texttt{Touring} Weight Function (\texttt{TWF})]%
  \label{def:twf}%
  % Let $\cdot$ denote the vector scalar product operator, the subscript
  % $^T$ denote the vector transposition operator,
  %
  Let $\leadsto$ % $\wedge$%_{\mathcal{S}_T}$
  denote the path-length operator between two skills in the taxonomy
  $\mathcal{S}_T$. %
  % , and
  % $\texttt{max}$ %_{\mathcal{S}_\mathcal{T}}$
  % denote the maximum path length.
  Then, \(\mathtt{TWF}\colon \mathcal{T}\times
  \widetilde{\mathcal{P}}\to\mathbb{R}\) is defined as follows:
  % $\leftrightarrow$
  \begin{equation}
    \label{eq:twf}
    \mathtt{TWF} (t, \widetilde{p}) =
    \frac{ %
      \sum_{\forall i} \sum_{\forall j} (t[i] \wedge \widetilde{p}[j]) \times ( s_i \leadsto s_j )
    } %
    { %
      \sum_{\forall i} \widetilde{p}[i] \times \sum_{\forall i} t[i] %
    }
  \end{equation}
\end{definition}

\subsection{Post-Assignment Phase}%
\label{sec:post}
%
\vspace{-3pt}%
%

% Two issues remain once the assignment is performed by the
% platfom. First, 
Workers need a secure way to fetch their own assignment. This can be
solved easily by well-known technical means. For example, the platform
could post the assignments on the Web (\emph{e.g.,} to a dedicated
webpage for each perturbed profile)
% that would consist in the secure hash of the worker's perturbed
% profile.
so that each worker would then access it through a secure web browser
(\emph{e.g.,} TOR\footnote{\url{https://www.torproject.org/}}).
% Second, when the $k$-\textsc{FlipMatch} and $k$-\textsc{GroupMatch}
% variants are used, a single worker must be elected out of the $k$
% workers assigned. Well-known secure solutions to this problem exist
% as well. For example, a usual secure set intersection protocol (see,
% \emph{e.g.,} the early proposal in~\cite{FreedmanNP04}) can allow a
% requester to select the worker with the best profile.

% We have two issues here: reaching the workers and dealing with false
% positives.

%%%%%%
% Issue #1 : reaching the workers %
%
% Post a mail or send to TOR. %
% This last step is not in the scope of this paper but a possible
% approach could use an anonymous network. For example, each
% participant's ID can be used to generate a temporary web page on a
% Tor node\footnote{\url{https://www.torproject.org/}} containing the
% contact information needed to establish a contact with the assigned
% participant. The worker-task participant could be kept private and
% secure with a specific communication suite such as
% GnuPG\footnote{\url{https://www.gnupg.org/}}.


% The overall scenario that our approaches will follow is
% straightforward and is close to a non-private task assignment. It is
% abstracted on figure~\ref{fig:scenario} \lb{\textbf{TODO}
%   (\textbf{LB})}. Workers and tasks send a perturbed version of their
% profile which is used by the platform to compute the task
% assignment. The resulting assignment is then cleaned-up by a
% post-processing routine such as Majority voting before being privately
% published to the participants. This last step is not in the scope of
% this paper but a possible approach could use an anonymous network. For
% example, each participant's ID can be used to generate a temporary web
% page on a Tor node\footnote{\url{https://www.torproject.org/}}
% containing the contact information needed to establish a contact with
% the assigned participant. The worker-task participant could be kept
% private and secure with a specific communication suite such as
% GnuPG\footnote{\url{https://www.gnupg.org/}}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "dexa17-short-CR"
%%% End: 
