\documentclass[10pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usetheme{metropolis}

\usepackage{tikz}%
\usetikzlibrary{positioning}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{arrows}
\usetikzlibrary{shapes.geometric, arrows}
\usetikzlibrary{decorations.pathreplacing,angles,quotes,arrows,shadows,fit,calc}%

\usepackage[english]{babel}
\usepackage{csquotes}

\colorlet{darkgreen}{green!50!black}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{lmodern}

\usepackage[style=alphabetic,]{biblatex}
\addbibresource{../papier-dexa17/crowdguard-rndresp-VLIGHT.bib}

\title{Lightweight Privacy-Preserving Task Assignment in Skill-Aware Crowdsourcing}
\date{April 6, 2017}
\author{Louis Béziaud, Tristan Allard, and David Gross-Amblard}
\institute{University of Rennes 1 / IRISA / École normale supérieure de Rennes}

\begin{document}
\maketitle

\section{Introduction}

\begin{frame}{Introduction --- Crowdsourcing}
  \begin{block}{Crowdsourcing}
    \textquote[]{Crowdsourcing represents the act of a company or institution taking a function once performed by employees and \textbf{outsourcing} it to an undefined (and generally \textbf{large}) network of people in the form of an \textbf{open call}.}\footnote{Jeff Howe, Wired Magazine, 2006}
    \begin{itemize}
      \item Wikipedia, Galaxy Zoo, Amazon Mechanical Turk, \dots
    \end{itemize}
  \end{block}

  \begin{block}{Knowledge-Intensive Crowdsourcing~\cite{BasuRoy2015}}
    Some tasks \textbf{requires} particular \textbf{skills} to be completed
    \begin{itemize}
      \item a translation from English to French
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Introduction --- Privacy}
  \begin{block}{Knowledge \(\implies\) Privacy Issues}
    Features can be \textbf{quasi-identifiers} / \textbf{sensitive data} :
    \begin{itemize}
      \item unique skill, location, availability, \dots
    \end{itemize}
    The platform is \textbf{not} a \textbf{trusted third party}
  \end{block}

  \begin{alertblock}{Example of Data Breach}
    A worker ID on mTurk gives access to the Amazon profile: real name, wish lists, book reviews, tagged products, \dots~\cite{LeaseHullmanBighamEtAl2013}
  \end{alertblock}

  % \begin{block}{Privacy-Accuracy Trade-off \cite{Hay:2016:PED:2882903.2882931}}
  %   Entities want to \textbf{optimize} the assignment in a \textbf{privacy-preserving} way
  % \end{block}
\end{frame}

\begin{frame}{Introduction -- Related Work}
  \begin{itemize}
    \item Task assignment using a maximum flow algorithm inside a \textbf{Paillier homomorphic cryptosystem}~\cite{Kajino2016} \\
    \emph{More than a century would be needed to compute an assignment between a hundred workers and a hundred tasks}
    \item Privacy-preserving crowdsourced surveys~\cite{KandappuSivaramanFriedmanEtAl2014} and spatial crowdsourcing~\cite{ToGhinitaShahabi2014} \\
    \emph{No skills, no assignment}
  \end{itemize}

\end{frame}

\section{Background}

\begin{frame}{Skills Taxonomy}
  \begin{figure}[h]
    \centering
    \resizebox{0.8\linewidth}{!}{%
      \begin{tikzpicture}[
        taxo/.style={
          shape=rectangle,
          rounded corners,
          draw,
          align=center,
          top color=white,
          bottom
          color=black!10
        },
        auto,
        level 1/.style={sibling distance=9em},
        level 2/.style={sibling distance=5em},
        ]
        \node[taxo] {$\perp$} child {
          node[taxo] (Sport) {Sport} child {
            node[taxo] (Yoga) {Yoga} child {
              node[taxo] (YogaH) {Hata Yoga}} child {
              node[taxo] (YogaO) {\ldots}
            }
          } child {
            node[taxo] (SportO) {\ldots}
          }
        } child {
          node[taxo] (CS) {CS} child {
            node[taxo] (Enc) {Crypto.}  child {
              node[taxo] (EncFHE) {FHE}
            } child {
              node[taxo] (EncO) {\ldots}
            }
          } child {
            node[taxo] (CSO) {\ldots}
          }
        } child {
          node[taxo] (H) {Health} child {
            node[taxo] (HAM) {Alt.}  child {
              node [taxo] (HAMO) {\ldots}
            } child {
              node [taxo] (HAMC) {Chemo. relief}
            }
          } child {
            node[taxo] (HO) {\ldots}
          }
        };
        %
        \node[align=left,anchor=west] at (-7, 0) (superskills) {\textbf{super-skills}};
        \node[align=left,anchor=west] at (-7, -3.9) (skills) {\textbf{skills} \textbf{$\mathcal{S}$}};
        %
        \node[draw,dashed,inner sep=0.1cm,fit={
          (skills) (YogaH) (YogaO) (EncFHE) (EncO) (HAMO) ($(HAMC.east)+(4pt,0)$)
        }] (skills) {};
        \node[draw,dashed,inner sep=0.1cm,fit={
          (superskills) (CSO) (Sport) (Yoga) (SportO) (CS) (Enc) (H) (HO) (HAM)}] (superskills) {};
      \end{tikzpicture}
    }
  \end{figure}
  \begin{itemize}
    \item \textbf{skill} \(s \in \{0, 1\}\)
    \item \textbf{superskill} \(s\): \(\mathtt{score}(s) = \) proportion of descendant skills possessed
  \end{itemize}
\end{frame}

\begin{frame}{Differential Privacy~\cite{DBLP:conf/icalp/Dwork06}}
  \begin{block}{Idea}
    The \textbf{outcome of any analysis is essentially equally likely}, independent of whether an individual joins, or refrains from joining, the data set
  \end{block}

  \begin{alertblock}{Differential Privacy~\cite{DBLP:conf/icalp/Dwork06}}
    \(M\) gives \(\epsilon\)-differential privacy if for all pairs of data-sets \(x\), \(y\) differing in one element, and all subsets \(S\) of possible outputs, \[\Pr[M(x) \in S] \leq e^\epsilon \Pr[M(y) \in S]\]
  \end{alertblock}
  \begin{block}{Properties}
    \begin{itemize}
      \item Sequential \& parallel \textbf{composition}~\cite{DBLP:conf/sigmod/McSherry09} \(\implies\) budget \(\epsilon\) sharing
      \item Post-Processing~\cite{DworkRoth2014} \(\implies\) can re-use data and add information
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Task Assignment Problem}
  \begin{itemize}
    \item \textbf{workers} \(\mathcal{P} = \{p_0, p_1, \dots, p_n\}\)
    \item \textbf{tasks} \(\mathcal{T} = \{t_0, t_1, \dots, t_n\}\)
    \item \textbf{weight function} \(\mathrm{wf} \colon \mathcal{P} \times \mathcal{T} \to \mathbb{R}\)
  \end{itemize}

  Find a bijection \(f\colon \mathcal{P} \to \mathcal{T}\) such that \(\sum_{p\in \mathcal{P}} \mathrm{wf}(p, f(p))\) is minimized.
\end{frame}

\begin{frame}{Assignment Quality}
  % total cost is weight-function-dependent!

  \begin{block}{Relative Quality}
    \[
      \mathtt{q_{rel}} %(\widetilde{\mathcal{A}}, \mathcal{A})
      =
      \frac{\sum_{{(t,p) \in \mathcal{A}}} \mathtt{C} (t,p)}
      {\sum_{{(t,\tilde{p}) \in \tilde{\mathcal{A}}}}
        \mathtt{C} (t,\tilde{p})}
    \]
  \end{block}

  \begin{block}{Fraction of Perfect Assignments}
    \[
      \mathtt{fpa} %(\widetilde{\mathcal{A}}, \mathcal{A})
      = \frac{\lvert \{ \tilde{p} \geq t \mid (t,\tilde{p}) \in \tilde{\mathcal{A}} \} \rvert}{\lvert\tilde{\mathcal{A}}\rvert}
    \]
  \end{block}
\end{frame}

\section{Contribution}

\begin{frame}[t]{Approach}
  \begin{center}
    \includegraphics[height=0.7\textheight]{approach.png}
  \end{center}
\end{frame}

\begin{frame}{Randomized Response~\cite{Warner1965}}
  \begin{figure}[h]
    \centering
    \resizebox{!}{0.7\textheight}{% minimum width=3cm, minimum height=0.5cm
      \tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
      \tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=green!30]
      \tikzstyle{arrow} = [thick,->,>=stealth]
      \begin{tikzpicture}[node distance=4cm,every node/.style={font=\large},]
        \node (in1) [io] {\(x\)};
        \node (dec1) [decision, below of=in1] {\(\mathtt{rand}() > \Pr_{flip}\)};
        \node (out1) [io, right of=dec1, xshift=2cm] {\(x\)};
        \node (dec2) [decision, below of=dec1] {\(\mathtt{rand}() > \Pr_{inno}\)};
        \node (out2) [io, right of=dec2, xshift=2cm] {\(0\)};
        \node (out3) [io, below of=dec2] {\(1\)};
        %
        \draw [arrow] (in1) -- (dec1);
        \draw [arrow] (dec1) -- node[anchor=north] {yes} (out1);
        \draw [arrow] (dec1) -- node[anchor=east] {no} (dec2);
        \draw [arrow] (dec2) -- node[anchor=north] {yes} (out2);
        \draw [arrow] (dec2) -- node[anchor=east] {no} (out3);
      \end{tikzpicture}
    }

    The randomized response mechanism satisfies \(\epsilon\)-differential-privacy if \(\Pr_{flip} = \frac{2}{1 + e^\epsilon}\)
  \end{figure}
  % \begin{itemize}
  %   \item[\(Q_S =\)] ``Have you ever shoplifted?'' \texttt{YES N0}
  %   \item[\(Q_I =\)] ``Flip a coin. Did you get a head?'' \texttt{YES NO}
  % \end{itemize}
\end{frame}

\begin{frame}{Weight Functions --- Existing Weight Functions}
  \begin{block}{Missing Weight Function \footnotesize{(\(\sim\) Hamming)}}
    \[
      \mathtt{MWF} (t, \tilde{p}) =
      \sum_{s_i \in t} \neg \tilde{p}[i]
    \]
  \end{block}

  \begin{block}{Ancestors Weight Function \footnotesize{(from~\cite{MavridisGross-AmblardMiklos2016})}}
    \[
      \mathtt{AWF}(t, \tilde{p}) = {
        \sum_{s_i \in \tilde{p}} {
          \min_{s_j \in t} \left(
            \frac{d_{max} - depth(lca(s_i, s_j))}{d_{max}}
          \right)
        }
      }
    \]
  \end{block}
\end{frame}

\begin{frame}[t]{Weight Functions --- Leveraging the Taxonomy}
  \vspace*{25pt}%
  \only<1->{
    \begin{minipage}[t]{.5\linewidth}
      \begin{block}{Climbing Weight Function \(\updownarrow\)} % vertical
        \[
          \mathtt{CWF} (t, \tilde{p}) = \sum_{\text{~level~} i} \textcolor{green}{i \times} \mathtt{d} (\textcolor{blue}{u_i}, \textcolor{red}{v_i})
        \]
        \begin{center}
          % \resizebox{1\linewidth}{!}{%
          \scalebox{0.4}{%
            \begin{tikzpicture}[
              taxo/.style={
                shape=rectangle,
                rounded corners,
                draw,
                align=center,
                top color=white,
                bottom color=black!10,
                minimum width=0.5cm,
                minimum height=0.05cm,
              },
              auto,
              level 1/.style={sibling distance=7em},
              level 2/.style={sibling distance=3em},
              ]
              \node[taxo] at (0, 0) (root) {} child {
                node[taxo] (A1) {} child {
                  node[taxo] (B1) {}
                } child {
                  node[taxo] (B2) {}
                }
              } child {
                node[taxo] (A2) {} child {
                  node[taxo] (B3) {}
                } child {
                  node[taxo] (B4) {}
                }
              };
              \node[taxo] at (4.7, 0) (root2) {} child {
                node[taxo] (A12) {} child {
                  node[taxo] (B12) {}
                } child {
                  node[taxo] (B22) {}
                }
              } child {
                node[taxo] (A22) {} child {
                  node[taxo] (B32) {}
                } child {
                  node[taxo] (B42) {}
                }
              };
              \node[color=blue] at (0, 0.9) (task) {\Huge $t$};
              \node[color=red] at (4.7, 0.9) (profile) {\Huge $\tilde{p}$};
              %
              \node[color=green] at (-3.5, 0) {\Huge $0 \times$};
              \node[color=green] at (-3.5, -1.5) {\Huge $1 \times$};
              \node[color=green] at (-3.5, -3) {\Huge $2 \times$};
              %
              \node[] at (-2.5, 0) (d1) {\Huge $\mathtt{d}($};
              \node[] at (-2.5, -1.5) (d2) {\Huge $\mathtt{d}($};
              \node[] at (-2.5, -3) (d3) {\Huge $\mathtt{d}($};
              %
              \node[] at (7.05, 0) (p1) {\Huge $)$};
              \node[] at (7.05, -1.5) (p2) {\Huge $)$};
              \node[] at (7.05, -3) (p3) {\Huge $)$};
              %
              \node[] at (2.3, -0.2) (c1) {\Huge ,};
              \node[] at (2.3, -1.65) (c2) {\Huge ,};
              \node[] at (2.3, -3.15) (c3) {\Huge ,};
              %
              \node[] at (7.5, -0.7) (pl1) {\Huge $+$};
              \node[] at (7.5, -2.2) (pl2) {\Huge $+$};
              %
              \node[taxo,opacity=0,above = 2.7cm of B1] (l0r) {};
              \node[taxo,opacity=0,above = 2.7cm of B4] (l0l) {};
              \node[taxo,opacity=0,above = 2.7cm of B12] (l0r2) {};
              \node[taxo,opacity=0,above = 2.7cm of B42] (l0l2) {};
              %
              \node[taxo,opacity=0,above = 1.25cm of B1] (l1r) {};
              \node[taxo,opacity=0,above = 1.25cm of B4] (l1l) {};
              \node[taxo,opacity=0,above = 1.25cm of B12] (l1r2) {};
              \node[taxo,opacity=0,above = 1.25cm of B42] (l1l2) {};
              %
              \node[draw,dashed,fill=blue,fill opacity=0.1,color=blue,inner sep=0.1cm,fit={(l0l) (root) (l0r)}] (l0) {};
              \node[draw,dashed,fill=blue,fill opacity=0.1,color=blue,inner sep=0.1cm,fit={(l1l) (A1) (A2) (l1r)}] (l1) {};
              \node[draw,dashed,fill=blue,fill opacity=0.1,color=blue,inner sep=0.1cm,fit={(B1) (B2) (B3) (B4)}] (l2) {};
              \node[draw,dashed,fill=red,fill opacity=0.1,color=red,inner sep=0.1cm,fit={(l0l2) (root2) (l0r2)}] (l02) {};
              \node[draw,dashed,fill=red,fill opacity=0.1,color=red,inner sep=0.1cm,fit={(l1l2) (A12) (A22) (l1r2)}] (l12) {};
              \node[draw,dashed,fill=red,fill opacity=0.1,color=red,inner sep=0.1cm,fit={(B12) (B22) (B32) (B42)}] (l22) {};
            \end{tikzpicture}
          }
        \end{center}
      \end{block}
    \end{minipage}}%
  \only<2->{%
    \hfill%
    \begin{minipage}[t]{0.5\linewidth}
      \begin{block}{Touring Weight Function \(\leftrightarrow\)} % horizontal
        \[
          \mathtt{TWF} (t, \tilde{p}) =
          \sum_{\textcolor{blue}{s_i} \in t \circ \tilde{p}}\ \sum_{\textcolor{red}{s_j} \in \tilde{p}}\ ( s_i \textcolor{green}{\leadsto} s_j )
        \]
        \begin{center}
          % \resizebox{!}{0.4\textheight}{%
          % \resizebox{0.6\linewidth}{!}{%
          \scalebox{0.4}{%
            \begin{tikzpicture}[
              taxo/.style={
                shape=rectangle,
                rounded corners,
                draw,
                align=center,
                top color=white,
                bottom color=black!10,
                minimum width=0.5cm,
                minimum height=0.05cm,
              },
              auto,
              % level distance=15pt,
              level 1/.style={sibling distance=14em},
              level 2/.style={sibling distance=6em},
              ]
              \node[taxo] at (0, 0) (root) {} child {
                node[taxo] (A1) {} child {
                  node[taxo,bottom color=red,top color=red] (B1) {}
                } child {
                  node[taxo,bottom color=blue,top color=blue] (B2) {}
                }
              } child {
                node[taxo] (A2) {} child {
                  node[taxo,bottom color=red, top color=red] (B3) {}
                } child {
                  node[taxo] (B4) {}
                }
              };
              % \node[] at (-1.5, -0.5) (profile) {\Huge $\tilde{p}$};
              %
              \path[->] (B2) edge [ultra thick,color=green,decorate,decoration={snake,amplitude=.2mm,post length=1mm},bend left=90,looseness=2] node[yshift=-2mm,color=green] {\Huge 2} (B1);
              \path[->] (B2) edge [ultra thick,color=green,decorate,decoration={snake,amplitude=.2mm,post length=1mm},bend right=90,looseness=2] node[yshift=2mm,color=green] {\Huge 4} (B3);
              \path[->] (B2) edge [ultra thick,bend right=90,looseness=2,dashed] node {\Huge 4} (B4);
            \end{tikzpicture}
          }
        \end{center}
      \end{block}
    \end{minipage}
  }
\end{frame}

\section{Evaluation}

\begin{frame}{Taxonomies}
  \textbf{Synthetic} taxonomy: perfect tree of height 3, and branching factor of 4, containing 121 nodes of which 81 are leaves

  \vspace{5pt}

  \begin{center}
    \includegraphics[height=0.5\textheight]{skillproject} \\
    \textbf{Real} taxonomy from Skill-Project\footnote{\url{http://en.skill-project.org/skills/}}, containing 2208 nodes, 1562 skills
  \end{center}
\end{frame}

\begin{frame}{Data sets}
  Three \textbf{synthetic} data sets:

  \vspace{5pt}

  Bernoulli  1\% and 10\%

  \vspace{5pt}

  \begin{center}
    \includegraphics[width=0.5\textwidth]{normal81} \\
    $\mathcal{N}ormal$ distribution
  \end{center}
\end{frame}

\begin{frame}{Results}
  \centering
  \tiny{taxonomy = Perfect} \\
  \includegraphics[height=0.28\textheight]{{fig6.qrel}.png} \\
  \includegraphics[height=0.28\textheight]{{fig6.qabs}.png} \\
  \tiny{taxonomy = SkillProject, data set = Normal} \\
  \includegraphics[height=0.28\textheight]{{fig7.qrel}.png}%
  \includegraphics[height=0.28\textheight]{{fig7.qabs}.png}
\end{frame}
\section{Conclusion}

\begin{frame}{Conclusion}
  \begin{itemize}
    \item \textbf{Lightweight} approach (\textbf{client-side})
    \item Pluggable into existing platforms
  \end{itemize}
\end{frame}

\appendix

\begin{frame}[allowframebreaks]{References}
  % \bibliography{../papier-dexa17/crowdguard-rndresp-VLIGHT.bib}
  \printbibliography%
  % \bibliographystyle{plain}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
